#!/bin/bash

echo "[INFO] Building apps library"
# cheap hack to enable building using our preferred cross toolchain in
# px4-build-docker
cp /usr/include/modal_journal.h /home/4.1.0.4/tools/linaro64/include/
mkdir -p apps/build
cd apps/build
cmake -DCMAKE_C_COMPILER="/home/4.1.0.4/tools/linaro64/bin/aarch64-linux-gnu-gcc" \
	  -DCMAKE_CXX_COMPILER="/home/4.1.0.4/tools/linaro64/bin/aarch64-linux-gnu-g++" \
	  -DCMAKE_LD="/home/4.1.0.4/tools/linaro64/bin/aarch64-linux-gnu-ld" \
	  -DCMAKE_LINKER="/home/4.1.0.4/tools/linaro64/bin/aarch64-linux-gnu-ld" ..
make -j$(nproc)
cd ../..

echo "[INFO] Building SLPI library"
mkdir -p slpi/build
cd slpi/build
cmake -DCMAKE_C_COMPILER="/home/4.1.0.4/tools/HEXAGON_Tools/8.4.05/Tools/bin/hexagon-clang" ..
make -j$(nproc)
cd ../..

echo "[INFO] Done!"

