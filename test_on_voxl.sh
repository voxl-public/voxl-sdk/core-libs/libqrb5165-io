VERSION=$(cat pkg/control/control | grep "Version" | cut -d' ' -f 2)

set -e

echo "[INFO] Building deb"
docker run --rm -it --net=host --privileged -w /home/ari/work --volume=/dev/bus/usb:/dev/bus/usb -e LOCAL_USER_ID=0 -e LOCAL_USER_NAME=root -e LOCAL_GID=0 -v $(pwd):/home/root/work:rw -w /home/root/work rb5-flight-px4-build-docker:v1.4 sh -c './install_build_deps.sh qrb5165 dev && ./build.sh && ./make_package.sh'

echo "[INFO] Waiting for device to appear"
adb wait-for-device

echo "[INFO] Pushing deb to voxl"
adb push libqrb5165-io_${VERSION}_arm64.deb /

echo "[INFO] Installing deb on voxl"
adb shell "apt-get install --reinstall -y --allow-downgrades /libqrb5165-io_${VERSION}_arm64.deb"

echo "[INFO] Stopping px4"
adb shell "systemctl stop voxl-px4"

echo "[INFO] Running tests"
adb shell "voxl_io_gtest" $@
