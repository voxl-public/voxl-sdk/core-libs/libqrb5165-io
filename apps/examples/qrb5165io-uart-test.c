/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <voxl_io.h>

#define ERROR(fmt, ...) fprintf(stderr, "[ERROR] " fmt "\n", ##__VA_ARGS__)

static void _help(){
    printf("Description: runs hardware loopback test on specified bus\n\n");
    printf("Usage: qrb5165io-uart-test\n");
    printf("\n");
    printf("    -d <device ID> (eg 3 for /dev/ttyHS3)\n");
    printf("\n");
}

// performs a basic whoami read from bus 3
int main (int argc, char *argv[]) {
    int opt = 0;
    int device = 0;	// default. /dev/ttyHS0
    int baud = 115200;

    //parsing command line options
    while ((opt = getopt(argc, argv, "d:h")) != -1){
        switch (opt){
            case 'b':
                baud = atoi(optarg);
                break;
            case 'd':
                device = atoi(optarg);
                break;

            case 'h':
            default:
                _help();
                return -1;
        }
    }

    if (device != APPS_HS1_UART && device != APPS_HS2_UART && device != APPS_HS3_UART && device != APPS_HS0_UART) {
        ERROR("Invalid UART device: got %d, was expecting %d, %d, %d, or %d", device,
              APPS_HS1_UART, APPS_HS2_UART, APPS_HS3_UART, APPS_HS0_UART);
        return 1;
    }

    int ret = voxl_uart_init(device, baud, 1.0, 0, 1, 0);
    if (ret < 0) {
        ERROR("Failed to open port");
        return 1;
    }

    const int MIN_WRITE_LENGTH = 32;
    uint8_t  write_buf[32];
    uint8_t  read_buf[32];
    int      len = 0;
    int      res = 0;
    // uint8_t  i = 255;

    for (uint8_t i = 255; i > 0; i--) {
        // dummy data
        memset(write_buf, i, MIN_WRITE_LENGTH);
        res = voxl_uart_write(device, write_buf, MIN_WRITE_LENGTH);
        if(res < 0){
            ERROR("Failed to write to UART");
            return 1;
        }
        res = voxl_uart_drain(device);
        if (res < 0) {
            ERROR("Failed to drain UART");
            return 1;
        }

        usleep(5);
        memset(read_buf, 0x00, MIN_WRITE_LENGTH);

        len = voxl_uart_read_bytes(device, read_buf, MIN_WRITE_LENGTH);
        if(len < 0){
            ERROR("Failed to read from UART");
            return 2;
        }
        if(len != MIN_WRITE_LENGTH){
            ERROR("Failed to read - expected 32 bytes, read %i instead", len);
            return 3;
        }
        else {
            for(int j = 0; j < 32; j++){
                if(read_buf[j] != write_buf[j]){
                    ERROR("Data integrity check failed on byte %d: expected %d, got %d", j, write_buf[j], read_buf[j]);
                    return 4;
                }
            }
        }

    }

    ret = voxl_uart_close(device);
    if(ret < 0){
        ERROR("Failed to close UART device");
        return 1;
    }
    printf("[INFO]: success!\n");
}

