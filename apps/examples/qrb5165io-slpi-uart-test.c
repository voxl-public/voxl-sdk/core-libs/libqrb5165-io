/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <voxl_io.h>

#define ERROR(fmt, ...) fprintf(stderr, "[ERROR] " fmt "\n", ##__VA_ARGS__)

static void _help(){
    printf("Description: runs hardware loopback test on specified bus on SLPI.\n\n");
    printf("Usage: qrb5165io-slpi-uart-test\n");
    printf("\n");
    printf("    -d <device ID>\tMust be 2, 6, or 7, corresponding to the SLPI UARTSs on\n"
           "                  \tports QUP2, QUP6, and QUP7. Defaults to 7 if not specificed.\n"
           "                  \tSee https://docs.modalai.com/voxl2-linux-user-guide/#uarts for more info.\n"
           "    -b <baud rate>\tSome standard UART baud rate, e.g. 115200\n"
           "    -i <# iter>   \tThe number of iterations of the test  to perform (default 1)\n");
    printf("\n");
}


static int perform_loopback_test(int bus) {
    const int32_t MIN_WRITE_SIZE = 32;
    uint8_t  write_buf[MIN_WRITE_SIZE];
    uint8_t  read_buf[MIN_WRITE_SIZE];
    int      len = 0;
    int      res = 0;

    for (uint8_t i = 255; i > 0; i--) {
        // dummy data
        memset(write_buf, i, MIN_WRITE_SIZE);
        res = voxl_uart_write(bus, write_buf, MIN_WRITE_SIZE);
        if(res < 0){
            ERROR("Failed to write bytes");
            return 1;
        }

        usleep(13000);
        memset(read_buf, 0x00, MIN_WRITE_SIZE);

        len = voxl_uart_read_bytes(bus, read_buf, MIN_WRITE_SIZE);
        if(len < 0){
            ERROR("Failed to read");
            return 2;
        }
        if (len != MIN_WRITE_SIZE) {
            ERROR("Failed to read - expected %d bytes, read %i instead", MIN_WRITE_SIZE, len);
            return 3;
        }
        else {
            for(int j = 0; j < MIN_WRITE_SIZE; j++){
                if(read_buf[j] != write_buf[j]){
                    // printf("Read: %i\n", read_buf[j]);
                    ERROR("Data integrity check failed");
                    return 4;
                }
            }
        }
    }

    return 0;
}

static int qup_to_bus(int qup) {
    switch (qup) {
        case 2:
            return SLPI_QUP2_UART;
        case 6:
            return SLPI_QUP6_UART;
        case 7:
            return SLPI_QUP7_UART;
        default:
            return -1;
    }
}


// performs a basic whoami read from bus 7
int main (int argc, char* argv[]) {
    int opt = 0;
    int device = 7;	    // default, /dev/slpi-uart-7
    int baud = 420000;  // default
    int num_test_iter = 1;

    //parsing command line options
    while ((opt = getopt(argc, argv, "d:b:i:h")) != -1){
        switch (opt) {
            case 'b':
                baud = atoi(optarg);
                break;

            case 'd':
                device = atoi(optarg);
                break;

            case 'i':
                num_test_iter = atoi(optarg);
                break;

            case 'h':
            default:
                _help();
                return -1;
        }
    }

    printf("Using device %d", device);
    int bus = qup_to_bus(device);
    if (bus == -1) {
        ERROR("Invalid device specified: %d (must be 2, 6, or 7)", device);
        return 1;
    }

    int ret = voxl_uart_init(bus, baud, 1.0, 0, 1, 0);
    if(ret < 0) {
        ERROR("Failed to open device");
        return 1;
    }

    printf("Iterating loopback test %d times\n", num_test_iter);
    for (int i = 0; i < num_test_iter; i++) {
        int rval = perform_loopback_test(bus);
        if (rval != 0) {
            ERROR("Test %d failed with error code %d", i+1, rval);
            return rval;
        }
    }

    ret = voxl_uart_close(bus);
    if(ret < 0) {
        ERROR("Failed to close port");
        return 1;
    }
    printf("[INFO]: success!\n");
}
