/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <voxl_io.h>

#define DEFAULT_SPI_BUS 3 // default /dev/spidev3.0 internal IMU on M0054

static void _help() {
    printf("Description: checks WHOAMI address of ICM42688p IMU or tests SPI loopback\n\n");
    printf("Usage: qrb5165io-spi-test\n");
    printf("\n");
    printf("    -d <device ID> (eg 3 for /dev/spidev3.0)\n");
    printf("    -t             Perform loopback test\n");
    printf("    -h             Display this help message\n");
    printf("\n");
}

static int _ping_icm42688p(int device) {
    int ret;

    uint8_t WHOAMI_REG = 0x75;
    uint8_t WHOAMI_VAL = 0x47;
    uint8_t DIR_READ = 0x80;
    uint8_t REG_BANK_SEL = 0x76;

    uint8_t tx_buf[4];
    memset(tx_buf, 0x00, 4);

    uint8_t rx_buf[4];
    memset(rx_buf, 0x00, 4);

    // select bank 0
    tx_buf[0] = REG_BANK_SEL;
    tx_buf[1] = 0;  // bank0
    tx_buf[2] = WHOAMI_REG | DIR_READ;
    tx_buf[3] = 0;  // keep clock going to read out

    // select bank 0, whoami
    ret = voxl_spi_transfer(device, tx_buf, 4, rx_buf);

    if (ret == 0 && rx_buf[3] == WHOAMI_VAL) {
        return 0;
    } else {
        return -1;
    }
}

static int _spi_loopback_test(int device) {
    int ret;

    uint8_t tx_buf[4] = {0xAA, 0x55, 0xFF, 0x00};
    uint8_t rx_buf[4];
    memset(rx_buf, 0x00, sizeof(rx_buf));

    ret = voxl_spi_transfer(device, tx_buf, sizeof(tx_buf), rx_buf);
    if (ret < 0) {
        printf("SPI transfer failed\n");
        return -1;
    }

    printf("Transmitted data: ");
    for (int i = 0; i < 4; i++) {
        printf("%02X ", tx_buf[i]);
    }
    printf("\n");

    printf("Received data: ");
    for (int i = 0; i < 4; i++) {
        printf("%02X ", rx_buf[i]);
    }
    printf("\n");

    for (int i = 0; i < 4; i++) {
        if (tx_buf[i] != rx_buf[i]) {
            printf("Mismatch at byte %d\n", i);
            return -1;
        }
    }

    printf("[INFO] Loopback test successful\n");
    return 0;
}

// basic WHOAMI read or loopback test
int main(int argc, char *argv[]) {
    int opt = 0;
    int device = DEFAULT_SPI_BUS; // default device
    int loopback_test = 0;        // flag for loopback test

    // parsing command line options
    while ((opt = getopt(argc, argv, "d:th")) != -1) {
        switch (opt) {
            case 'd':
                device = atoi(optarg);
                break;
            case 't':
                loopback_test = 1;
                break;
            case 'h':
            default:
                _help();
                return -1;
        }
    }

    int ret = voxl_spi_init(device, SPI_MODE_0, VOXL_SPI_MAX_SPEED);
    if (ret < 0) {
        printf("Failed to open SPI port\n");
        return 1;
    }

    if (loopback_test) {
        printf("[INFO] Performing SPI loopback test\n");
        ret = _spi_loopback_test(device);
    } else {
        printf("[INFO] Checking WHOAMI address of ICM42688p IMU\n");
        ret = _ping_icm42688p(device);
        if (ret < 0) {
            printf("Failed to ping ICM42688p IMU\n");
        } else {
            printf("[INFO] Success! ICM42688 Detected\n");
        }
    }

    ret = voxl_spi_close(device);
    if (ret < 0) {
        printf("Failed to close SPI port\n");
        return 1;
    }

    return 0;
}
