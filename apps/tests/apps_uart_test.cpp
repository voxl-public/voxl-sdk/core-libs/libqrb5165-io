#include <chrono>
#include <thread>

#include "voxl_io/uart.h"
#include "gtest/gtest.h"
#include "common.h"

using std::chrono::steady_clock;
using std::chrono::seconds;
using std::chrono::milliseconds;
using std::chrono::duration_cast;


TEST(AppsUartTests, test_open_single_uart_many_times) {
    int rval = voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval =  voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, 0);
}

TEST(AppsUartTests, test_open_many_close_then_open) {
    int rval = voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_init(APPS_HS1_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS1_UART);
    ASSERT_EQ(rval, 0);
}


TEST(AppsUartTests, test_open_close_single_uart) {
    int rval = voxl_uart_init(APPS_HS0_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS0_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(APPS_HS0_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS0_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(APPS_HS0_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS0_UART);
    EXPECT_EQ(rval, 0);
}


TEST(AppsUartTests, test_open_close_many_uarts) {
    int rval = voxl_uart_init(APPS_HS0_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(APPS_HS1_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(APPS_HS2_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(APPS_HS3_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    rval = voxl_uart_close(APPS_HS0_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS1_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS2_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(APPS_HS3_UART);
    EXPECT_EQ(rval, 0);
}


// this test requires a loopback plug installed on the TTYHS2 UART
TEST(AppsUartTests, test_simple_loopback) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the /dev/ttyHS2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");
    int device = APPS_HS2_UART;
    int baud = 921600;
    int ret = voxl_uart_init(device, baud, 1.0, 0, 1, 0);
    EXPECT_EQ(ret, 0);

    const int MIN_WRITE_LENGTH = 32;
    uint8_t  write_buf[32];
    uint8_t  read_buf[32];
    int      len = 0;
    int      res = 0;
    // uint8_t  i = 255;

    for (uint8_t i = 255; i > 0; i--) {
        // dummy data
        memset(write_buf, i, MIN_WRITE_LENGTH);
        res = voxl_uart_write(device, write_buf, MIN_WRITE_LENGTH);
        EXPECT_GT(res, 0);
        EXPECT_EQ(res, MIN_WRITE_LENGTH);

        res = voxl_uart_drain(device);
        EXPECT_EQ(res, 0);

        usleep(50);
        memset(read_buf, 0x00, MIN_WRITE_LENGTH);

        len = voxl_uart_read_bytes(device, read_buf, MIN_WRITE_LENGTH);
        EXPECT_GT(len, 0);
        EXPECT_EQ(len, MIN_WRITE_LENGTH);

        for(uint8_t& item : read_buf){
            EXPECT_EQ(item, i);
        }

    }

    ret = voxl_uart_close(device);
    EXPECT_EQ(ret, 0);
}


TEST(AppsUartTests, test_timeout_no_data) {
    int rval = voxl_uart_init(APPS_HS2_UART, 921600, 1.0, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    uint8_t buf[256];
    auto start_time = steady_clock::now();
    rval = voxl_uart_read_bytes(APPS_HS2_UART, buf, 255);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();

    EXPECT_EQ(rval, 0);
    EXPECT_NEAR(duration, 1000, 50);

    rval = voxl_uart_close(APPS_HS2_UART);
    EXPECT_EQ(rval, 0);

    rval = voxl_uart_init(APPS_HS2_UART, 921600, 0.25, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    start_time = steady_clock::now();
    rval = voxl_uart_read_bytes(APPS_HS2_UART, buf, 1);
    end_time = steady_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();

    EXPECT_EQ(rval, 0);
    EXPECT_NEAR(duration, 250, 5);
    rval = voxl_uart_close(APPS_HS2_UART);
    EXPECT_EQ(rval, 0);
}


TEST(AppsUartTests, test_all_valid_baud) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the /dev/ttyHS2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int bauds[] = { 9600, 19200, 38400, 57600, 115200, 230400, 460800, 500000, 576000, 921600, 1000000, 1152000, 2000000,
                    2500000, 3000000, 3500000, 4000000 };

    const int MIN_WRITE_LENGTH = 32;
    for (int& baud : bauds) {
        test_print("Testing %d baud", baud);

        int rval = voxl_uart_init(APPS_HS2_UART, baud, 0.0f, false, 1, false);
        ASSERT_EQ(rval, 0);

        uint8_t  write_buf[32];
        uint8_t  read_buf[32];
        int      len = 0;
        int      res = 0;
        // uint8_t  i = 255;

        for (uint8_t i = 255; i > 0; i--) {
            // dummy data
            memset(write_buf, i, MIN_WRITE_LENGTH);
            res = voxl_uart_write(APPS_HS2_UART, write_buf, MIN_WRITE_LENGTH);
            EXPECT_GT(res, 0);
            EXPECT_EQ(res, MIN_WRITE_LENGTH);

            res = voxl_uart_drain(APPS_HS2_UART);
            EXPECT_EQ(res, 0);

            usleep(50);
            memset(read_buf, 0x00, MIN_WRITE_LENGTH);

            len = voxl_uart_read_bytes(APPS_HS2_UART, read_buf, MIN_WRITE_LENGTH);
            EXPECT_GT(len, 0);
            EXPECT_EQ(len, MIN_WRITE_LENGTH);

            for(uint8_t& item : read_buf){
                EXPECT_EQ(item, i);
            }

            res = voxl_uart_flush(APPS_HS2_UART);
            ASSERT_EQ(res, 0);

        }

        rval = voxl_uart_close(APPS_HS2_UART);
        ASSERT_EQ(rval, 0);
    }
}


// this test requires a loopback cable installed on ttyHS2
TEST(AppsUartTests, test_timeout_partial_data) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the /dev/ttyHS2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int rval = voxl_uart_init(APPS_HS2_UART, 921600, 2.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    auto handle = std::thread([]() {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        uint64_t data = 0xDEADBEEF;
        int res = voxl_uart_write(APPS_HS2_UART, reinterpret_cast<uint8_t*>(&data), 8);
        EXPECT_EQ(res, 8);
    });

    auto start_time = steady_clock::now();
    uint8_t buf[32];
    rval = voxl_uart_read_bytes(APPS_HS2_UART, buf, 16);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();

    handle.join();

    EXPECT_EQ(rval, 8);
    EXPECT_NEAR(duration, 1000, 5);
    EXPECT_EQ(*reinterpret_cast<uint64_t*>(buf), 0xDEADBEEF);

    rval = voxl_uart_close(APPS_HS2_UART);
    EXPECT_EQ(rval, 0);
}


TEST(AppsUartTests, test_no_timeout_blocks_long) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the /dev/ttyHS2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int rval = voxl_uart_init(APPS_HS2_UART, 921600, 0, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    auto handle = std::thread([]() {
        std::this_thread::sleep_for(std::chrono::seconds(15));
        uint64_t data = 0xDEADBEEF;
        int res = voxl_uart_write(APPS_HS2_UART, reinterpret_cast<uint8_t*>(&data), 8);
        EXPECT_EQ(res, 8);
    });

    auto start_time = steady_clock::now();
    uint8_t buf[32];
    rval = voxl_uart_read_bytes(APPS_HS2_UART, buf, 8);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();

    handle.join();

    EXPECT_EQ(rval, 8);
    EXPECT_NEAR(duration, 15000, 5);
    EXPECT_EQ(*reinterpret_cast<uint64_t*>(buf), 0xDEADBEEF);

    rval = voxl_uart_close(APPS_HS2_UART);
    EXPECT_EQ(rval, 0);
}
