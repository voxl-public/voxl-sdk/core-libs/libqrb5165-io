//
// Created by ari on 9/6/23.
//

#ifndef LIBQRB5165_IO_COMMON_H
#define LIBQRB5165_IO_COMMON_H

#define test_print(fmt, ...) printf("\033[0;32m[   INFO   ] \033[90m" fmt "\033[0m\n", ##__VA_ARGS__);

#endif //LIBQRB5165_IO_COMMON_H
