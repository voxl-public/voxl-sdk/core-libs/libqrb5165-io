//
// Created by ari on 9/5/23.
//

#include <chrono>
#include <thread>

#include "voxl_io/uart.h"
#include "gtest/gtest.h"
#include "common.h"

using std::chrono::steady_clock;
using std::chrono::seconds;
using std::chrono::milliseconds;
using std::chrono::duration_cast;


TEST(SlpiUartTests, test_open_single_uart_many_times) {
    int rval = voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval =  voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval =  voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, 0);
}

TEST(SlpiUartTests, test_open_many_close_then_open) {
    int rval = voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, -1);
    rval = voxl_uart_init(SLPI_QUP7_UART, 4000000, 1.0, 0, 1, 0);
    ASSERT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP7_UART);
    ASSERT_EQ(rval, 0);
}



TEST(SlpiUartTests, test_open_close_single_uart) {
    int rval = voxl_uart_init(SLPI_QUP6_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP6_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(SLPI_QUP6_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP6_UART);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_init(SLPI_QUP6_UART, 921600, 0.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);
    rval = voxl_uart_close(SLPI_QUP6_UART);
    EXPECT_EQ(rval, 0);
}


// this test requires a loopback plug installed on the TTYHS2 UART
TEST(SlpiUartTests, test_simple_loopback) {
    int device = SLPI_QUP2_UART;
    int baud = 921600;
    int ret = voxl_uart_init(device, baud, 1.0, 0, 1, 0);
    EXPECT_EQ(ret, 0);

    const int MIN_WRITE_LENGTH = 32;
    uint8_t  write_buf[32];
    uint8_t  read_buf[32];
    int      len = 0;
    int      res = 0;
    // uint8_t  i = 255;

    for (uint8_t i = 255; i > 0; i--) {
        // dummy data
        memset(write_buf, i, MIN_WRITE_LENGTH);

        auto start_time = steady_clock::now();
        res = voxl_uart_write(device, write_buf, MIN_WRITE_LENGTH);
        auto end_time = steady_clock::now();
        auto duration = duration_cast<milliseconds>(end_time - start_time).count();

        EXPECT_GT(res, 0);
        EXPECT_EQ(res, MIN_WRITE_LENGTH);

        usleep(50);
        memset(read_buf, 0x00, MIN_WRITE_LENGTH);

        start_time = steady_clock::now();
        len = voxl_uart_read_bytes(device, read_buf, MIN_WRITE_LENGTH);
        end_time = steady_clock::now();
        duration = duration_cast<milliseconds>(end_time - start_time).count();
        EXPECT_GT(len, 0);
        EXPECT_EQ(len, MIN_WRITE_LENGTH);

        for (int j = 0; j < 32; j++){
            EXPECT_EQ(read_buf[j], i);
        }

    }

    ret = voxl_uart_close(device);
    EXPECT_EQ(ret, 0);
}


TEST(SlpiUartTests, test_sequence_loopback) {
    int device = SLPI_QUP2_UART;
    int baud = 2000000;

    int res = voxl_uart_init(device, baud, 1.0, 0, 1, 0);
    EXPECT_EQ(res, 0);

    const size_t BUF_SIZE = 16;
    uint32_t write_buf[BUF_SIZE];
    uint32_t read_buf[BUF_SIZE];
    const int WRITE_LENGTH = BUF_SIZE * sizeof(uint32_t);
    int len = 0;


    // 2000000 32-bit values is approximately 64MB of data
    for (uint32_t i = 0; i < 2000000; i += BUF_SIZE) {
        for (int j = 0; j < BUF_SIZE; j++) {
            write_buf[j] = i + j;
        }


        auto start_time = steady_clock::now();
        res = voxl_uart_write(device, reinterpret_cast<uint8_t const*>(write_buf), WRITE_LENGTH);
        auto end_time = steady_clock::now();
        auto duration = duration_cast<milliseconds>(end_time - start_time).count();

        EXPECT_GT(res, 0);
        EXPECT_EQ(res, WRITE_LENGTH);

        usleep(50);
        memset(read_buf, 0x00, WRITE_LENGTH);

        start_time = steady_clock::now();
        len = voxl_uart_read_bytes(device, reinterpret_cast<uint8_t*>(read_buf), WRITE_LENGTH);
        end_time = steady_clock::now();
        duration = duration_cast<milliseconds>(end_time - start_time).count();
        EXPECT_GT(len, 0);
        EXPECT_EQ(len, WRITE_LENGTH);


        for (uint32_t j = 0; j < BUF_SIZE; j++) {
            EXPECT_EQ(read_buf[j], write_buf[j]);
        }

        if (i % 100000 == 0) {
            test_print("iteration %d / 2000000, duration was %ld ms", i, duration);
        }
    }

    res = voxl_uart_close(device);
    EXPECT_EQ(res, 0);
}



TEST(SlpiUartTests, test_timeout_no_data) {
    int rval = voxl_uart_init(SLPI_QUP2_UART, 921600, 1.0, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    uint8_t buf[256];
    auto start_time = steady_clock::now();
    rval = voxl_uart_read_bytes(SLPI_QUP2_UART, buf, 255);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();
    test_print("Total duration spend on timeout 1 (slpi) was %ld ms", duration);

    EXPECT_EQ(rval, 0);
    EXPECT_NEAR(duration, 1000, 50);

    rval = voxl_uart_close(SLPI_QUP2_UART);
    EXPECT_EQ(rval, 0);

    rval = voxl_uart_init(SLPI_QUP2_UART, 921600, 0.25, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    start_time = steady_clock::now();
    rval = voxl_uart_read_bytes(SLPI_QUP2_UART, buf, 1);
    end_time = steady_clock::now();
    duration = duration_cast<milliseconds>(end_time - start_time).count();
    test_print("Total duration spend on timeout 2 (slpi) was %ld ms", duration);

    EXPECT_EQ(rval, 0);
    EXPECT_NEAR(duration, 250, 5);
    rval = voxl_uart_close(SLPI_QUP2_UART);
    EXPECT_EQ(rval, 0);
}
//
//
//// this test requires a loopback cable installed on SLPI QUP2 uart
//TEST(SlpiUartTests, test_timeout_partial_data) {
//
//}

TEST(SlpiUartTests, test_all_valid_baud) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the QUP2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int bauds[] = { 9600, 38400, 57600, 115200, 230400, 250000, 420000, 460800, 921600,  1843200, 2000000, };

    const int MIN_WRITE_LENGTH = 32;
    for (int& baud : bauds) {
        test_print("Testing %d baud", baud);

        int rval = voxl_uart_init(SLPI_QUP2_UART, baud, 0.0f, false, 1, false);
        ASSERT_EQ(rval, 0);

        uint8_t  write_buf[32];
        uint8_t  read_buf[32];
        int      len = 0;
        int      res = 0;
        // uint8_t  i = 255;

        for (uint8_t i = 255; i > 0; i--) {
            // dummy data
            memset(write_buf, i, MIN_WRITE_LENGTH);
            res = voxl_uart_write(SLPI_QUP2_UART, write_buf, MIN_WRITE_LENGTH);
            EXPECT_GT(res, 0);
            EXPECT_EQ(res, MIN_WRITE_LENGTH);

            usleep(50);
            memset(read_buf, 0x00, MIN_WRITE_LENGTH);

            len = voxl_uart_read_bytes(SLPI_QUP2_UART, read_buf, MIN_WRITE_LENGTH);
            EXPECT_GT(len, 0);
            EXPECT_EQ(len, MIN_WRITE_LENGTH);

            for(uint8_t& item : read_buf){
                EXPECT_EQ(item, i);
            }
        }

        rval = voxl_uart_close(SLPI_QUP2_UART);
        ASSERT_EQ(rval, 0);
    }
}
// this test requires a loopback cable installed on ttyHS2
TEST(SlpiUartTests, test_timeout_partial_data) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the QUP2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int rval = voxl_uart_init(SLPI_QUP2_UART, 921600, 2.5, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    auto handle = std::thread([]() {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        uint64_t data = 0xDEADBEEF;
        int res = voxl_uart_write(SLPI_QUP2_UART, reinterpret_cast<uint8_t*>(&data), 8);
        EXPECT_EQ(res, 8);
    });

    auto start_time = steady_clock::now();
    uint8_t buf[32];
    rval = voxl_uart_read_bytes(SLPI_QUP2_UART, buf, 16);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();

    handle.join();

    EXPECT_EQ(rval, 8);
    EXPECT_NEAR(duration, 1000, 5);
    EXPECT_EQ(*reinterpret_cast<uint64_t*>(buf), 0xDEADBEEF);

    rval = voxl_uart_close(SLPI_QUP2_UART);
    EXPECT_EQ(rval, 0);
}

TEST(SlpiUartTests, test_bytes_available) {
    uint8_t data[] = { 0x01, 0xF1, 0x04, 0xF4, 0x15, 0xDE, 0xAD, 0xBE, 0xEF};
    auto const data_size_bytes = sizeof(data);

    int device = SLPI_QUP2_UART;
    int baud = 2000000;
    auto res = voxl_uart_init(device, baud, 0, 0, 1, 0);
    ASSERT_EQ(res, 0);

    res = voxl_uart_write(device, data, data_size_bytes);
    ASSERT_EQ(res, data_size_bytes);
    usleep(1000);

    int bytes_avail = voxl_uart_bytes_available(device);
    EXPECT_EQ(bytes_avail, data_size_bytes);

    res = voxl_uart_write(device, data, data_size_bytes);
    ASSERT_EQ(res, data_size_bytes);
    usleep(1000);
    bytes_avail = voxl_uart_bytes_available(device);
    EXPECT_EQ(bytes_avail, data_size_bytes*2);

    uint8_t data2[] = {0xF0, 0x0D};
    res = voxl_uart_write(device, data2, 2);
    ASSERT_EQ(res, 2);
    usleep(1000);

    bytes_avail = voxl_uart_bytes_available(device);
    EXPECT_EQ(bytes_avail, data_size_bytes*2+2);

    res = voxl_uart_read_bytes(device, data, data_size_bytes);
    ASSERT_EQ(res, data_size_bytes);
    usleep(1000);

    bytes_avail = voxl_uart_bytes_available(device);
    EXPECT_EQ(bytes_avail, data_size_bytes+2);

    res = voxl_uart_close(device);
    ASSERT_EQ(res, 0);
}

TEST(SlpiUartTests, test_no_timeout_blocks_long) {
    test_print("WARNING: The following test will fail without a loopback cable installed on the QUP2 UART");
    std::this_thread::sleep_for(std::chrono::seconds(1));
    test_print("Proceeding with test...");

    int rval = voxl_uart_init(SLPI_QUP2_UART, 921600, 0, 0, 1, 0);
    EXPECT_EQ(rval, 0);

    auto handle = std::thread([]() {
        std::this_thread::sleep_for(std::chrono::seconds(15));
        uint64_t data = 0xDEADBEEF;
        int res = voxl_uart_write(SLPI_QUP2_UART, reinterpret_cast<uint8_t*>(&data), 8);
        EXPECT_EQ(res, 8);
    });

    auto start_time = steady_clock::now();
    uint8_t buf[32];
    rval = voxl_uart_read_bytes(SLPI_QUP2_UART, buf, 8);
    auto end_time = steady_clock::now();
    auto duration = duration_cast<milliseconds>(end_time - start_time).count();
    usleep(1000);

    handle.join();

    EXPECT_EQ(rval, 8);
    EXPECT_NEAR(duration, 15000, 5);
    EXPECT_EQ(*reinterpret_cast<uint64_t*>(buf), 0xDEADBEEF);

    rval = voxl_uart_close(SLPI_QUP2_UART);
    EXPECT_EQ(rval, 0);
}
