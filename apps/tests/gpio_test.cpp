//
// Created by ari on 9/22/23.
//
#include <thread>
#include <chrono>

#include "gtest/gtest.h"
#include "common.h"
#include "voxl_io/gpio.h"
#include <modal_journal.h>

using std::this_thread::sleep_for;
using std::chrono::seconds;
using std::chrono::milliseconds;

TEST(GpioTests, test_set_direction_succeeds_rval) {
    int rval = voxl_gpio_set_direction(0, OUTPUT);
    ASSERT_EQ(rval, 0);
    rval = voxl_gpio_set_direction(0, INPUT);
    ASSERT_EQ(rval, 0);
}

TEST(GpioTests, test_set_level_succeeds_rval) {
    voxl_gpio_set_direction(0, OUTPUT);
    int rval = voxl_gpio_write(0, 1);
    ASSERT_EQ(rval, 0);

    rval = voxl_gpio_write(0, 0);
    ASSERT_EQ(rval, 0);
}

TEST(GpioTests, test_set_reads_correct_value) {
    const int OUTPUT_PIN = 0;
    const int INPUT_PIN = 8;
    test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
               INPUT_PIN, OUTPUT_PIN);

    int rval = voxl_gpio_set_direction(OUTPUT_PIN, OUTPUT);
    ASSERT_EQ(rval, 0);
    rval = voxl_gpio_set_direction(INPUT_PIN, INPUT);
    ASSERT_EQ(rval, 0);

    rval = voxl_gpio_write(OUTPUT_PIN, 1);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));

    int read_val = voxl_gpio_read(INPUT_PIN);
    ASSERT_EQ(read_val, 1);

    rval = voxl_gpio_write(OUTPUT_PIN, 0);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));
    read_val = voxl_gpio_read(INPUT_PIN);
    ASSERT_EQ(read_val, 0);
}

TEST(GpioTests, test_gpio_switch_directions) {
    int output_pin = 0;
    int input_pin = 8;
    test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
               input_pin, output_pin);

    int rval = voxl_gpio_set_direction(output_pin, OUTPUT);
    ASSERT_EQ(rval, 0);
    rval = voxl_gpio_set_direction(input_pin, INPUT);
    ASSERT_EQ(rval, 0);

    rval = voxl_gpio_write(output_pin, 1);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));

    int read_val = voxl_gpio_read(input_pin);
    ASSERT_EQ(read_val, 1);

    rval = voxl_gpio_write(output_pin, 0);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));
    read_val = voxl_gpio_read(input_pin);
    ASSERT_EQ(read_val, 0);

    output_pin = 8;
    input_pin = 0;

    rval = voxl_gpio_set_direction(output_pin, OUTPUT);
    ASSERT_EQ(rval, 0);
    rval = voxl_gpio_set_direction(input_pin, INPUT);
    ASSERT_EQ(rval, 0);

    rval = voxl_gpio_write(output_pin, 1);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));

    read_val = voxl_gpio_read(input_pin);
    ASSERT_EQ(read_val, 1);

    rval = voxl_gpio_write(output_pin, 0);
    ASSERT_EQ(rval, 0);
    sleep_for(milliseconds(100));
    read_val = voxl_gpio_read(input_pin);
    ASSERT_EQ(read_val, 0);
}

TEST(GpioTests, test_hammer_gpio_write) {
    const int OUTPUT_PIN = 8;
    const int INPUT_PIN = 0;
    const int NUM_ITER = 15000;
    test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
               INPUT_PIN, OUTPUT_PIN);

    int rval = voxl_gpio_set_direction(OUTPUT_PIN, OUTPUT);
    ASSERT_EQ(rval, 0);
    rval = voxl_gpio_set_direction(INPUT_PIN, INPUT);
    ASSERT_EQ(rval, 0);

    for (int i = 0; i < NUM_ITER; i++) {

        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        int read_val = voxl_gpio_read(INPUT_PIN);
        ASSERT_EQ(read_val, 1);

        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);
        read_val = voxl_gpio_read(INPUT_PIN);
        ASSERT_EQ(read_val, 0);

    }
}

namespace interrupt_tests {

    std::function<void(uint32_t, int)> g_lambda_callback; // Global std::function
    extern "C" void c_callback(uint32_t pin, int state) {
        g_lambda_callback(pin, state);
    }

    TEST(GpioTests, test_new_interrupt_handler_rising_edge) {
        M_JournalSetLevel(VERBOSE);
        const int OUTPUT_PIN = 0;
        const int INPUT_PIN = 8;
        test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
                   INPUT_PIN, OUTPUT_PIN);

        int rval = voxl_gpio_set_direction(OUTPUT_PIN, OUTPUT);
        ASSERT_EQ(rval, 0);
        rval = voxl_gpio_set_direction(INPUT_PIN, INPUT);
        ASSERT_EQ(rval, 0);

        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);

        uint32_t callback_counter = 0;
        g_lambda_callback = [&](uint32_t pin, int state) {
            test_print("Got callback for pin %u (state = %d)", pin, state);
            callback_counter += 1;
        };
        rval = voxl_gpio_set_edge_callback(INPUT_PIN, RISING, c_callback);
        ASSERT_EQ(rval, 0);

        // generate 2 rising edges using output pin
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);

        sleep_for(milliseconds(100));
        ASSERT_EQ(callback_counter, 2);
        rval = voxl_gpio_remove_edge_callback(INPUT_PIN);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(100));
    }

    TEST(GpioTests, test_new_interrupt_handler_falling_edge) {
        M_JournalSetLevel(VERBOSE);
        const int OUTPUT_PIN = 0;
        const int INPUT_PIN = 8;
        test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
                   INPUT_PIN, OUTPUT_PIN);

        int rval = voxl_gpio_set_direction(OUTPUT_PIN, OUTPUT);
        ASSERT_EQ(rval, 0);
        rval = voxl_gpio_set_direction(INPUT_PIN, INPUT);
        ASSERT_EQ(rval, 0);

        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);

        uint32_t callback_counter = 0;
        g_lambda_callback = [&](uint32_t pin, int state) {
            test_print("Got callback for pin %u (state = %d)", pin, state);
            callback_counter += 1;
        };
        rval = voxl_gpio_set_edge_callback(INPUT_PIN, FALLING, c_callback);
        ASSERT_EQ(rval, 0);

        // generate 3 falling edges using output pin
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);

        sleep_for(milliseconds(100));
        ASSERT_EQ(callback_counter, 3);
        rval = voxl_gpio_remove_edge_callback(INPUT_PIN);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(100));
    }

    TEST(GpioTests, test_new_interrupt_handler_both_edges) {
        M_JournalSetLevel(VERBOSE);
        const int OUTPUT_PIN = 0;
        const int INPUT_PIN = 8;
        test_print("Note -- the following test will fail without a jumper wire connecting Gpio pins %d and %d",
                   INPUT_PIN, OUTPUT_PIN);

        int rval = voxl_gpio_set_direction(OUTPUT_PIN, OUTPUT);
        ASSERT_EQ(rval, 0);
        rval = voxl_gpio_set_direction(INPUT_PIN, INPUT);
        ASSERT_EQ(rval, 0);

        uint32_t callback_counter = 0;
        g_lambda_callback = [&](uint32_t pin, int state) {
            test_print("Got callback for pin %u (state = %d)", pin, state);
            callback_counter += 1;
        };
        rval = voxl_gpio_set_edge_callback(INPUT_PIN, BOTH, c_callback);
        ASSERT_EQ(rval, 0);

        // HACK: I think an edge is getting "queued up" from a previous test, and is causing the counter to
        // increment here before it should. so, we'll wait a moment and then set callback_counter to 0 before
        // we start generating edges again
        sleep_for(milliseconds(100));
        callback_counter = 0;
        sleep_for(milliseconds(100));
        ASSERT_EQ(callback_counter, 0);

        // generate 3 rising/falling edges using output pin
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 0);
        ASSERT_EQ(rval, 0);
        sleep_for(milliseconds(10));
        rval = voxl_gpio_write(OUTPUT_PIN, 1);
        ASSERT_EQ(rval, 0);

        sleep_for(milliseconds(100));
        ASSERT_EQ(callback_counter, 3);
        rval = voxl_gpio_remove_edge_callback(INPUT_PIN);
        ASSERT_EQ(rval, 0);
    }
}
