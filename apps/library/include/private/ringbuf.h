//
// Created by ari on 9/14/23.
//

#ifndef LIBQRB5165_IO_RINGBUF_H
#define LIBQRB5165_IO_RINGBUF_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <pthread.h>

/**
 * This file implements a thread-safe FIFO ringbuffer with a constant size per-instance. Additionally, it implements
 * blocking and timeout reads -- reads will block indefinitely, or for a maximum timeout.
 */

/**
 * This struct defines a ring buffer structure. Members should not be directly accessed externally.
 */
typedef struct {
    size_t head; /// the position where the next byte to be written should go
    size_t tail; /// the position where the next byte to be read resides

    size_t size; /// the number of bytes currently contained within the ring buf
    size_t max_size; /// the max number of bytes which can be stored within the ring buf

    pthread_mutex_t mutex; /// a mutex protecting this struct
    pthread_cond_t data_cond; /// a condition variable used to block until data is received when the buf is empty

    uint8_t buf[]; /// the buffer containing the actual data
} ring_buf_t;

typedef enum ring_buf_status {
    OK = 0,
    ERR_NO_SPACE = -1,
    ERR_OTHER = -2,
} ring_buf_status_t;


char const* ring_buf_status_str(ring_buf_status_t status);


/**
 * Create a ring buffer, initialized to the correct state.
 *
 * @param max_size The maximum number of elements which can be held in the ring buffer.
 * @return A pointer to the created ring buffer structure, or 0 on failure.
 */
ring_buf_t* ring_buf_create(size_t max_size);

/**
 * Push data into the back of the ring buffer. This will fail if there is not enough space to fit all the data.
 *
 * @param self the ring buffer to write to
 * @param data the data to write
 * @param length the length of the data
 * @return OK if sucecssful, or ERR_NO_SPACE (-1) if there is not sufficient space in which to fit all of the data
 */
ring_buf_status_t ring_buf_push(ring_buf_t* __restrict__ self, uint8_t const* __restrict__ data, size_t length);


/**
 * Pop data from the front of the ring buffer. This will read data up to `max_length`. It will return the number of
 * bytes read from the buffer. This method will block until some amount of data can be read.
 *
 * @param self the ring buffer to read from
 * @param dest_buf the buffer into which data should be read
 * @param max_length the maximum amount of data (in bytes) which can be read into the destination buffer
 * @return
 */
ring_buf_status_t ring_buf_pop(ring_buf_t* __restrict__ self, uint8_t* __restrict__ dest_buf, size_t max_length);


/**
 * Clear the contents of the given ring buffer
 * @param self the ring buffer to clear
 * @return whether or not the clear was successful
 */
ring_buf_status_t ring_buf_clear(ring_buf_t* self);


/**
 * Pop data from the front of the ringbuffer. This will read data up to `max_length` into `dest_buf`. This
 * method will block until some data is available, or until `timeout_ms` has been reached while waiting for data.
 * If timeout is zero, this method will return immediately.
 *
 * @param self the ring buffer to read from
 * @param dest_buf the buffer into which data should be read
 * @param max_length the maximum amount of data (in bytes) which can be read into the destination buffer
 * @param timeout_ms the amount of time to wait for data to be available
 * @return the number of bytes read, or some error (negative number) on failure
 */
ring_buf_status_t ring_buf_pop_timeout(ring_buf_t* __restrict__ self, uint8_t* __restrict__ dest_buf, size_t max_length,
                                       uint32_t timeout_ms);


/**
 * Get the size of the ring buffer
 * @param self The ring buffer whose size we want to retrieve.
 * @return the size of the given ring buffer
 */
size_t ring_buf_size(ring_buf_t* self);


/**
 * Get the number of free bytes in the given ring buffer
 * @param self the ring buffer
 * @return the number of free bytes
 */
size_t ring_buf_avail_size(ring_buf_t* self);


/**
 * Deallocate the given ring buffer
 * @param self the ring buffer to deallocate
 * @return the status of deallocation
 */
ring_buf_status_t ring_buf_free(ring_buf_t* self);

#ifdef __cplusplus
}     // extern "C"
#endif

#endif //LIBQRB5165_IO_RINGBUF_H
