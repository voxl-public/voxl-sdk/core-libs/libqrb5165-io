/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_IO_SPI_H
#define VOXL_IO_SPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/spi/spidev.h>  // for xfer and ioctl calls
#include <stdint.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////
// SPI
///////////////////////////////////////////////////////////////////////////////

#define VOXL_SPI_BITS_PER_WORD 8     ///< only allow for 8-bit words
#define VOXL_SPI_MAX_SPEED 24000000  ///< 24mhz
#define VOXL_SPI_MIN_SPEED 1000      ///< 1khz

/**
 * @brief      Initializes a SPI port a specified mode and frequency
 *
 *             For description of the 4 SPI bus modes, see
 *             <https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus#Mode_numbers>
 *
 *             Frequency is provided in hertz. MPU9250 claims to support up to 20mhz but we observed corrupted data. In practice 10mhz is plenty fast.
 *
 * @param[in]  bus       SPI bus number (BLSP #)
 * @param[in]  bus_mode  0,1,2, or 3
 * @param[in]  freq_hz   The frequency in hz
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_init(int bus, int bus_mode, int freq_hz);

/**
 * @brief      fetches the file descriptor for a specified bus so the user can
 * do more advanced IO operations than what's presented here
 *
 * @param[in]  bus    The bus
 *
 * @return     fd or -1 on failure
 */
int voxl_spi_get_fd(int bus);

/**
 * @brief      update the speed of the SPI port
 *
 *             Use this is you wish to run the bus slower for some operations
 *             and faster for others. For example register configuration may
 *             want to run more slowly for robustness but read in data quickly.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  freq_hz  The frequency in hz
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_set_freq(int bus, int freq_hz);

/**
 * @brief      closes a SPI bus
 *
 *             Please make an effort to call this before your process exits to
 *             help ensure a safe cleanup.
 *
 * @param[in]  bus   SPI bus number (BLSP #)
 *
 * @return     returns 0 on success, -1 on error.
 */
int voxl_spi_close(int bus);

/**
 * @brief      reads specified number of bytes out of a register
 *
 *             You cannot read/write more than DSPAL_SPI_RECEIVE_BUFFER_LENGTH (512)
 *             bytes at a time.
 *
 * @param[in]  bus        SPI bus number (BLSP #)
 * @param      tx_data    data to send out bus
 * @param[in]  tx_bytes   number of bytes to write
 * @param      rx_data    Buffer to write result into
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_transfer(int bus, uint8_t* tx_data, size_t tx_bytes, uint8_t* rx_data);

/**
 * @brief      writes data out the SPI bus
 *
 * data length can't exceed DSPAL_SPI_TRANSMIT_BUFFER_LENGTH (512)
 *
 * @param[in]  bus    SPI bus number (BLSP #)
 * @param      data   pointer to data to be written
 * @param[in]  bytes  number of bytes to send
 *
 * @return     0 on success -1 on failure
 */
int voxl_spi_write(int bus, uint8_t* data, size_t bytes);

/**
 * @brief      writes a single byte register at specified address
 *
 *             this works by writing the address followed by val which is how we
 *             write registers on the mpu9250 IMU. It may not work on your
 *             sensor.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  address  register address
 * @param[in]  val      The value to write
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_write_reg_byte(int bus, uint8_t address, uint8_t val);

/**
 * @brief      writes a single word (16 bit) register at specified address
 *
 *             this works by writing the address followed by val which is how we
 *             write registers on the mpu9250 IMU. It may not work on your
 *             sensor.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  address  register address
 * @param[in]  val      The value to write
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_write_reg_word(int32_t bus, uint8_t address, uint16_t val);

/**
 * @brief      Reads data from a specified bus
 *
 * @param[in]  bus    SPI bus to use
 * @param      data   data pointer
 * @param[in]  bytes  number of bytes to read
 *
 * @return     number of bytes read or -1 on failure
 */
int voxl_spi_read(int bus, uint8_t* data, size_t bytes);

/**
 * @brief      reads specified number of bytes out of a register
 *
 *             This works by sending the requested address with the MSB set to 1
 *             indicating a read operation on MPU9250 and similar IMUs. It then
 *             reads the specified number of bytes out. This is intended for MPU
 *             series IMUs and may not work with your sensor if it behaves
 *             differently.
 *
 *             If reading chunks of data you should allocate RPC shared memory
 *             with voxl_rpc_shared_mem_alloc(size_t bytes) to use for the
 *             output buffer.
 *
 *             You cannot read more than DSPAL_SPI_RECEIVE_BUFFER_LENGTH (512)
 *             bytes at a time.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  address  register address
 * @param      out_buf  pointer to buffer write the result into
 * @param[in]  length   The length of data to read in bytes
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_read_reg(int bus, uint8_t address, uint8_t* out_buf, int length);

/**
 * @brief      reads a single byte register
 *
 *             This works by sending the requested address with the MSB set to 1
 *             indicating a read operation on MPU9250 and similar IMUs. It then
 *             reads one byte out. This is intended for MPU series IMUs and may
 *             not work with your sensor if it behaves differently.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  address  register address
 * @param      out      pointer to where to write the result
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_read_reg_byte(int bus, uint8_t address, uint8_t* out);

/**
 * @brief      reads a single word (16-bit) register
 *
 *             This works by sending the requested address with the MSB set to 1
 *             indicating a read operation on MPU9250 and similar IMUs. It then
 *             reads two bytes out. This is intended for MPU series IMUs and may
 *             not work with your sensor if it behaves differently.
 *
 * @param[in]  bus      SPI bus number (BLSP #)
 * @param[in]  address  register address
 * @param      out      pointer to where to write the result
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_read_reg_word(int bus, uint8_t address, uint16_t* out);

/**
 * @brief      helper function to read out a regularly formed FIFO buffer
 *
 *             This is intended for the MPU series IMU FIFO buffers. The process
 *             consists of first reading the number of bytes available in the
 *             IMU's FIFO as read from the fifo_count register address. This
 *             count should be a multiple of packet_size.
 *
 *             The data (even multiple of packet_size) is then read out of the
 *             fifo_address register and placed into the out_buf buffer.
 *
 *             The user should allocate RPC shared memory with
 *             voxl_rpc_shared_mem_alloc(size_t bytes) to use for the output
 *             buffer with at least the size of the IMU's FIFO. Also provide the
 *             size of this buffer with the dataLen argument so the functions
 *             knows not to write too far and create an overflow. You should
 *             make sure your RPC shared mem and this length are appropriately
 *             sized for the fifo you are reading.
 *
 *             For example the icm42688 has a 2k buffer but also has additional
 *             read cache so it can technically hold 103 packets (2060) bytes.
 *             We use a 2k + 128 = 2170 bytes buffer in voxl-imu-server to be
 *             safe.
 *
 *             The user can specify an optinal minimum number of packets to
 *             read. If greater than 0, the function will not return until the
 *             fifo has at least min_packets available to read. This can help
 *             reduce CPU usage at high sample rates.
 *
 * @param[in]  bus            SPI bus number (BLSP #)
 * @param[in]  count_address  fifo_count register address
 * @param[in]  fifo_address   fifo_data address
 * @param[in]  packet_size    expected bytes per fifo packet
 * @param[out] packets_read   The number of packets read
 * @param[out] data           The output buffer (should be RPC shared memory)
 * @param[in]  dataLen        Size of the output buffer.
 * @param[in]  count_speed    SPI speed in hz to read fifo_count register
 * @param[in]  data_speed     SPI speed in hz to read the fifo data register
 *
 * @return     0 on success, -1 on failure
 */
int voxl_spi_read_imu_fifo(int bus, uint8_t count_address, uint8_t fifo_address,
                           uint8_t packet_size, int* packets_read, uint8_t* data,
                           int dataLen, int count_speed, int data_speed);

#ifdef __cplusplus
}
#endif

#endif  // VOXL_IO_SPI_H
