#ifndef VOXL_IO_GPIO_
#define VOXL_IO_GPIO_


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

// highest number of GPIO pin which can be used (note that actual available pin count may vary)
#define MAX_NUM_GPIO_PINS 180
#define GPIO_DIRECTION_INPUT 0
#define GPIO_DIRECTION_OUTPUT 1

typedef enum gpio_direction {
    INPUT = 0,
    OUTPUT = 1,
} gpio_direction_t;

/**
 * @brief      Read the state of a GPIO pin
 *
 *             The pin will be exported when calling this function
 *
 * @param[in]  gpio_pin       GPIO pin number
 *
 * @return     0 on success, -1 on failure
 */
int voxl_gpio_read(uint32_t gpio_pin);

/**
 * @brief      Set the state of a GPIO pin
 *
 *             The pin will be exported when calling this function
 *
 * @param[in]  gpio_pin       GPIO pin number
 * @param[in]  value          Desired value to write to pin (0=LOW or 1=High)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_gpio_write(uint32_t gpio_pin, uint8_t value);

/**
 * @brief      Set the direction of a GPIO pin
 *
 *             The pin will be exported when calling this function. Some pins have set directions
 *             that can't be changed.
 *
 * @param[in]  gpio_pin       GPIO pin number
 * @param[in]  direction      Desired direction of pin (0=IN or 1=OUT)
 *
 * @return     0 on success, -1 on failure
 */
int voxl_gpio_set_direction(uint32_t gpio_pin, uint8_t direction);

/**
 * interrupt_callback_t represents a function pointer which is intended to be used as ain interrupt callback. The first
 * argument will be the pin number on which the interrupt has occurred, and the second argument will be the pin state.
 */
typedef void (*interrupt_callback_t)(uint32_t, int);
typedef enum edge_type {
    FALLING = 0,
    RISING = 1,
    BOTH = 2,
} edge_type_t;
/**
 * @brief Set a handler for a particular GPIO pin which will be called when the given edge transition occurs. Only one
 *          handler callback can exist for each GPIO pin; if the given pin already contains an interrupt handler,
 *          it will be overwritten by this thread's. The interrupt handler will be called from a different thread
 *          context than this function is called from; apply synchronization as necessary.
 * @param gpio_pin the pin for which the handler will be set
 * @param edge the edge transition at which the handler will be called
 * @param callback the function which will be called to handle the transition event. first argument will be the pin
 *      on which the interrupt was triggered, second argument will be the pin state.
 * @return whether the interrupt handler was successfully registered (0 ok, -1 error)
 */
int voxl_gpio_set_edge_callback(uint32_t gpio_pin, edge_type_t edge, interrupt_callback_t callback);

/**
 * @brief Remove the registered edge callback for a particular GPIO pin. If there is no callback registered for the
 *          given pin, this function will have no effect.
 * @param gpio_pin the pin for which the callback should be removed
 * @return 0 if the callback is removed successfully, or -1 otherwise
 */
int voxl_gpio_remove_edge_callback(uint32_t gpio_pin);

#ifdef __cplusplus
}      // extern "C"
#endif

#endif // VOXL_IO_GPIO_
