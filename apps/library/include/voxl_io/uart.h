/**
 * From <rc/uart.h>
 *
 * @brief      C interface for the Linux UART driver
 *
 * This is a general-purpose C interface to the linux UART driver device
 * (/dev/ttyHS*). This is developed and tested on the BeagleBone platform but
 * should work on other linux systems too.
 *
 * @author     James Strawson
 * @date       3/6/2018
 *
 */

#ifndef VOXL_IO_UART_H
#define VOXL_IO_UART_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>


typedef enum voxl_uarts {
    APPS_HS0_UART = 0,
    APPS_HS1_UART = 1,
    APPS_HS2_UART = 2,
    APPS_HS3_UART = 3,
    SLPI_QUP2_UART = 12,
    SLPI_QUP6_UART = 16,
    SLPI_QUP7_UART = 17,

    NUM_UARTS = 7,
} voxl_uart_bus_t;


/**
 * @brief      Initializes a UART bus at specified baudrate and timeout. The bus
 * should be a member of the `voxl_uarts` enum.
 *
 * This is a very generalized function that configures the bus for 8-bit
 * characters and ignores the modem status lines.
 *
 * If you need a configuration other than whats presented here then you are
 * probably doing something fancy with the bus and you will probably want to do
 * your own reading/writing with standard Linux methods. Note that only "apps"
 * processor UARTS can be read and written with standard Linux methods.
 *
 * @param[in]  bus           The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 *
 * @param[in]  baudrate      must be one of the standard speeds in the UART
 * spec. 115200 and 57600 are most common.
 *
 * @param[in]  timeout       The number of seconds to wait when reading data.
 * If the timeout is set to zero, reads will block indefinitely until data appears.
 *
 * @param[in]  stop_bits     number of stop bits, 1 or 2, usually 1 for most
 * sensors. Applies only to UARTs located on the "apps" processor.
 *
 * @param[in]  parity_en     0 to disable parity, nonzero to enable. usually
 * disabled for most sensors. Applies only to UARTs located on the "apps"
 * processor.
 *
 * @return     0 on success, -1 on failure
 */
int voxl_uart_init(int bus, int baudrate, float timeout_sec, int canonical_en, int stop_bits, int parity_en);


// for SLPI UART ports specifically, the last 4 args of voxl_uart_init() are ignored
// You can use the full voxl_uart_init() function if you like, or you can use this
// simplified one. This function is used by voxl-esc and voxl-elrs for talking to
// the ESC and ELRS radios that are connected to SLPI and normally talk through PX4
int voxl_uart_init_legacy_slpi(int bus, int baudrate);


/**
 * @brief      closes a UART bus
 *
 * @param[in] bus  The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 *
 * @return         returns 0 on success, -1 on error.
 */
int voxl_uart_close(int bus);


/**
 * @brief       Returns the file discriptor to a uart bus after the bus has been
 * initialized. This method is only valid for UARTs located on the "apps"
 * processor.
 *
 * @param[in] bus  The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 *
 * @return         The file descriptor of the UART bus, or -1 on error.
*/
int voxl_uart_get_fd(int bus);


/**
 * @brief       Discards any data which has been received but not read, and any
 * data which has been written but not sent.
 *
 * @param[in] bus The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 * @return        0 on success or -1 on failure
*/
int voxl_uart_flush(int bus);


/**
 * @brief       Sends data through the uart
 *
 * @param[in] bus     The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 * @param[in] data    A pointer to the beginning of the data to be sent
 * @param[in] length  The number of bytes to be sent. Must be > 0.
 *
 * @return      The number of bytes sent, or -1 on error.
*/
int voxl_uart_write(int bus, uint8_t const* data, size_t length);


/**
 * @brief      reads bytes from the UART bus
 *
 * This is a blocking function call. It will return once either some number of bytes has been
 * read from the uart specified by `bus` (this may be less than `num_bytes`), or the timeout
 * set on this bus has expired.
 *
 * @param[in] bus         The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 * @param[out] buf        data pointer
 * @param[in]  num_bytes  The number of bytes to read. Must be > 0.
 *
 * @return     Returns number of bytes actually read or -1 on error.
 */
int voxl_uart_read_bytes(int bus, uint8_t* buf, size_t num_bytes);


/**
 * @brief      reads a line of characters ending in newline '\n'
 *
 * This is a blocking function call. It will only return on these conditions:
 *
 * - a newline character was read, this is discarded.
 * - max_bytes were read, this prevents overflowing a user buffer.
 * - timeout declared in voxl_uart_init() is reached
 * - shutdown flag is set by voxl_uart_close
 *
 * Note that this method can return without seeing a newline if the timeout is reached.
 * In this case, there will still be data in the buffer.
 *
 * @param[in] bus         The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 * @param[out] buf        data pointer
 * @param[in]  max_bytes  max bytes to read in case newline character not found
 *
 * @return     Returns number of bytes actually read or -1 on error.
 */
int voxl_uart_read_line(int bus, uint8_t* buf, size_t max_bytes);


/**
 * @brief      Fetches the number of bytes ready to be read from a bus
 *
 * @param[in] bus  The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 *
 * @return         Returns number of bytes ready to be read or -1 on error.
 */
int voxl_uart_bytes_available(int bus);


/**
 * @brief Set the read timeout for a specific UART bus.
 *
 * This function sets the read timeout for a specific UART bus. The timeout value determines how long
 * the UART will wait for a response before considering the operation as timeout.
 *
 * @param[in] bus  The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 * @param timeout The number of milliseconds at which the operation should time out, or zero to not time out.
 *
 * @return 0 on success or -1 on error
 */
int voxl_uart_set_read_timeout(int bus, uint32_t timeout);


/**
 * @brief Waits for all data written to the uart represented by the given bus to be transmitted.
 *
 * @param[in] bus  The bus number. Rather than specifying an index
 * manually, use members of the `voxl_uarts` enum.
 *
 * @return 0 on success, -1 on error
 */
int voxl_uart_drain(int bus);


#ifdef __cplusplus
}
#endif

#endif // VOXL_IO_UART_H

