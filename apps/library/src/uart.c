#include <stdbool.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <modal_journal.h>
#include <poll.h>
#include <sys/time.h>
#include <sys/ioctl.h>

#include "voxl_io/uart.h"
#include "private/fc_sensor.h"
#include "private/ringbuf.h"


#define VALIDATE_BUS(bus) /*NOLINTBEGIN*/ do { \
    if (bus < 0                \
        || (bus > 3 && bus < 12) \
        || bus > 17) { \
        M_ERROR("%s: Bus must be in the inclusive range 0-3 or 12-17 (got %d)\n", __FUNCTION__, \
                bus); \
        return -1; \
    } \
} while (0) /*NOLINTEND*/

#define CHECK_BUS_INIT(bus) /* NOLINTBEGIN */ do { \
    if (!config[bus].initialized) { \
        M_ERROR("%s: Bus '%d' is not initialized\n", __FUNCTION__, bus); \
        return -1; \
    } \
} while (0) /* NOLINTEND */


#define SLPI_RX_BUF_SIZE (8*1024)
#define SLPI_TIMEOUT_WAIT_S 0.1

#define MIN_APPS_READ_LENGTH 1

#define UNUSED __attribute__((unused))

// this should never occur, just pleases my linter since it can't find modal_journal.h for some reason
#ifndef M_VERBOSE
#define M_VERBOSE printf
#endif
#ifndef M_DEBUG
#define M_DEBUG printf
#endif
#ifndef M_WARN
#define M_WARN printf
#endif
#ifndef M_ERROR
#define M_ERROR printf
#endif

typedef enum {
    APPS,
    SLPI,
} uart_location_t;

typedef struct slpi_uart_config {
    uint32_t port_number;
    uint32_t baud_rate;
} slpi_uart_config_t;

typedef struct slpi_uart {
    slpi_uart_config_t conf;
} slpi_uart_t;

typedef struct apps_uart {
    int fd;
} apps_uart_t;

typedef struct common_uart_config {
    bool initialized;
    bool should_close;
    uint32_t baud;
    uint32_t timeout_millis;
    void* data;
} common_uart_config_t;


// cheap hack: expand bus to 17 so we can keep using enum values as raw idx
static common_uart_config_t config[18] = {0};

static ring_buf_t* slpi_buf = NULL;
static bool slpi_in_use = false;
static const bool enable_debug = false;
static pthread_mutex_t slpi_init_mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;


static uart_location_t get_uart_loc_from_bus(voxl_uart_bus_t bus) {
    switch (bus) {
        case APPS_HS0_UART:
        case APPS_HS1_UART:
        case APPS_HS2_UART:
        case APPS_HS3_UART:
            return APPS;

        case SLPI_QUP2_UART:
        case SLPI_QUP6_UART:
        case SLPI_QUP7_UART:
            return SLPI;

        default:
            M_ERROR("Unable to find location for bus %d, this should never happen!\n", bus);
            return APPS;
    }
}


static uint64_t get_time_millis(void) {
    struct timeval tv;

    gettimeofday(&tv,NULL);
    return (((uint64_t) tv.tv_sec) * 1000) + ((uint64_t) tv.tv_usec / 1000);
}


/**
 * @brief Callback function for receiving data from SLPI.
 *
 * This function is called when data is received from SLPI.
 * It copies the received data into the slpi_rx_buf for storage.
 *
 * @param topic: The topic on which the data is received.
 * @param data: The pointer to the received data.
 * @param len_bytes: The length of the received data in bytes.
 *
 * @return void
 *
 * @note This function assumes that slpi_in_use and slpi_rx_buf are properly initialized.
 *       It uses a mutex to guarantee thread safety while accessing the slpi_rx_buf.
 *       If the mutex cannot be acquired or released, error messages are printed.
 *       If the uart is not in use, a warning message is printed and the function returns without processing the data.
 *       The function copies the received data to the slpi_rx_buf in a circular buffer manner.
 */
static void p_slpi_receive_callback(char const* topic,
                                    uint8_t const* data,
                                    const uint32_t len_bytes) {
    if (!slpi_in_use) {
        M_WARN("Received %d bytes of data from SLPI while SLPI uart not in use (topic %s)\n", len_bytes, topic);
        return;
    }
    if (strncmp("uart_data", topic, 9) != 0) {
        M_WARN("Received data from SLPI for unrecognized topic %s, discarding data\n", topic);
        return;
    }

    ring_buf_status_t res = ring_buf_push(slpi_buf, data, len_bytes);
    if (res != OK) {
        char const* err = ring_buf_status_str(res);
        M_ERROR("Received error from ring_buf_push in p_slpi_receive_callback: %s\n", err);
    }
}


static void p_slpi_unused_callback(const char* topic) {
    M_WARN("Received unused callback for topic %s\n", topic);
}


static int p_slpi_init() {
    fc_callbacks cb = {
        .rx_callback         = &p_slpi_receive_callback,
        .ad_callback         = &p_slpi_unused_callback, // advertise
        .sub_callback        = &p_slpi_unused_callback, // add subscription
        .remove_sub_callback = &p_slpi_unused_callback, // remove subscription
    };

    if (fc_sensor_set_library_name("libslpi_qrb5165_io.so") != 0) {
        M_ERROR("Unable to set library name when initializing slpi\n");
        return -1;
    }

    if (fc_sensor_initialize(enable_debug, &cb) != 0) {
        M_ERROR("fc_sensor_initialize failed\n");
        return -1;
    }

    if (slpi_buf == NULL) {
        slpi_buf = ring_buf_create(SLPI_RX_BUF_SIZE);
    }
    if (slpi_buf == NULL) {
        return -1;
    }

    return 0;
}


static int p_bus_to_slpi_port(const int bus) {
    switch (bus) {
        case SLPI_QUP2_UART:
            return 2;
        case SLPI_QUP6_UART:
            return 6;
        case SLPI_QUP7_UART:
            return 7;
        default:
            return -1;
    }
}



static int p_slpi_uart_init(const int bus, const int baudrate, slpi_uart_t* self)
{
    static bool slpi_is_initialized = false;

    pthread_mutex_lock(&slpi_init_mutex);
    if (slpi_in_use) {
        M_ERROR("Unable to initialize more than one SLPI uart at a time. To use another SLPI uart, please close the open SLPI uart.\n");
        return -1;
    }

    if (!slpi_is_initialized) {
        M_VERBOSE("Initializing slpi\n");
        if (p_slpi_init() != 0) {
            M_ERROR("Failed to initialize slpi\n");
            return -1;
        }
        slpi_is_initialized = true;
        M_DEBUG("Successfully initialized SLPI\n");
    }
    else {
        M_VERBOSE("SLPI already initialized\n");
    }

    int maybe_slpi_port = p_bus_to_slpi_port(bus);
    if (maybe_slpi_port < 0) {
        M_ERROR("Unable to convert bus to slpi port, this should never happen\n");
        pthread_mutex_unlock(&slpi_init_mutex);
        return -1;
    }

    self->conf = (slpi_uart_config_t) {
        .baud_rate   = baudrate,
        .port_number = (uint32_t) maybe_slpi_port,
    };
    int rval = fc_sensor_send_data("uart_config", (const uint8_t*) &(self->conf),
                                   sizeof(slpi_uart_config_t));
    if (rval != 0) {
        M_ERROR("Unable to configure SLPI uart\n");
        return -1;
    }
    slpi_in_use = true;

    pthread_mutex_unlock(&slpi_init_mutex);
    return 0;
}

static speed_t p_baud_num_to_termios_speed(const int baud_num) {
    switch (baud_num) {
    case 4000000:
        return B4000000;
    case 3500000:
        return B3500000;
    case 3000000:
        return B3000000;
    case 2500000:
        return B2500000;
    case 2000000:
        return B2000000;
    case 1152000:
        return B1152000;
    case 1000000:
        return B1000000;
    case 921600:
        return B921600;
    case 576000:
        return B576000;
    case 500000:
        return B500000;
    case 460800:
        return B460800;
    case 230400:
        return B230400;
    case 115200:
        return B115200;
    case 57600:
        return B57600;
    case 38400:
        return B38400;
    case 19200:
        return B19200;
    case 9600:
        return B9600;
    case 4800:
        return B4800;
    case 2400:
        return B2400;
    case 1800:
        return B1800;
    case 1200:
        return B1200;
    case 600:
        return B600;
    case 300:
        return B300;
    case 200:
        return B200;
    case 150:
        return B150;
    case 134:
        return B134;
    case 110:
        return B110;
    case 75:
        return B75;
    case 50:
        return B50;
    default:
        M_ERROR("ERROR: int voxl_apps_uart_init, invalid baudrate. Please use a standard baudrate\n");
        return -1;
    }
}


static const char* p_bus_to_apps_dev(const int bus) {
    // only three options, so we can just switch for now
    switch (bus) {
        case APPS_HS0_UART:
            return "/dev/ttyHS0";
        case APPS_HS1_UART:
            return "/dev/ttyHS1";
        case APPS_HS2_UART:
            return "/dev/ttyHS2";
        case APPS_HS3_UART:
            return "/dev/ttyHS3";
        default:
            M_ERROR("Got bus which is not valid as an appsproc uart in p_bus_to_apps_dev, this should never happen\n");
            return "";
    }
}

static int p_apps_uart_init(const int bus, const int baud, const int canonical_en, const int stop_bits,
                            const int parity_en, apps_uart_t* self) {

    if (stop_bits != 1 && stop_bits != 2) {
        fprintf(stderr,"ERROR in voxl_uart_init, stop bits must be 1 or 2\n");
        return -1;
    }

    speed_t speed = p_baud_num_to_termios_speed(baud);
    const char* dev = p_bus_to_apps_dev(bus);
 

    int fd = open(dev, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd < 0) {
        M_ERROR("Unable to open device file '%s': received %d\n", dev, fd);
        perror("Unable to open device file\n");
        return -1;
    }

    struct termios term_config;
    if (tcgetattr(fd, &term_config) == -1) {
        M_ERROR("Unable to get attributes of device %s\n", dev);
        perror("Unable to get attributes of device\n");
        goto APPS_INIT_EXIT_ERR;
    }

    // not sure why we bother getting the config just to wipe it...
    // just matching the previous behavior here
    memset(&term_config, 0, sizeof(term_config));

    // also yanked from previous version
    // the following lines technically do nothing since we just wiped config
    // but they exist to allow easy fiddling and be more explicit about
    // which settings are in use


    term_config.c_lflag &= ~(ECHO | ECHONL | ISIG | IEXTEN);
    if (canonical_en)   term_config.c_lflag |= ICANON;
    else                term_config.c_lflag &= ~ICANON;
    if (parity_en)      term_config.c_lflag |= PARENB;
    else                term_config.c_lflag &= ~PARENB;
    if (stop_bits == 1) term_config.c_cflag &= ~CSTOPB;
    else                term_config.c_cflag |= CSTOPB;

    term_config.c_cflag &= ~CSIZE;	// wipe all size masks
    term_config.c_cflag |= CS8;		// set size to 8 bit characters
    term_config.c_cflag |= CREAD;	// enable reading
    term_config.c_cflag |= CLOCAL;	// ignore modem status lines

    term_config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                             INLCR | PARMRK | INPCK | ISTRIP | IXON);


    term_config.c_oflag = 0;

    term_config.c_cc[VMIN] = MIN_APPS_READ_LENGTH;
    // we will not set a timeout here -- will use poll instead
    term_config.c_cc[VTIME] = 0;


    if (cfsetispeed(&term_config, speed) != 0) {
        perror("Unable to set UART input speed when initializing apps uart\n");
        goto APPS_INIT_EXIT_ERR;
    }
    if (cfsetospeed(&term_config, speed) != 0) {
        perror("Unable to set UART output speed when initializing apps uart\n");
        goto APPS_INIT_EXIT_ERR;
    }

    // configure flush and set attributes
    if (tcflush(fd, TCIOFLUSH) != 0) {
        perror("Unable to call tcflush when initializing apps uart\n");
        goto APPS_INIT_EXIT_ERR;
    }
    if (tcsetattr(fd, TCSANOW, &term_config) != 0) {
        perror("Unable to call tcsetattr when initializing apps uart\n");
        goto APPS_INIT_EXIT_ERR;
    }
    if (tcflush(fd, TCIOFLUSH) != 0) {
        perror("Unable to call tcflush when initializing apps uart\n");
        goto APPS_INIT_EXIT_ERR;
    }

    // turn off the FNDELAY flag
    if(fcntl(fd, F_SETFL, 0) != 0) {
        perror("ERROR: in voxl_apps_uart_init calling fcntl");
        goto APPS_INIT_EXIT_ERR;
    }
    if(tcflush(fd, TCIOFLUSH) != 0) {
        perror("perror: in voxl_apps_uart_init calling tcflush");
        goto APPS_INIT_EXIT_ERR;
    }


    self->fd = fd;
    return 0;


APPS_INIT_EXIT_ERR:
    M_ERROR("Unable to initialize apps uart\n");
    close(fd);
    return -1;
}


int voxl_uart_init(int bus, int baud, float timeout_sec, int canonical_en, int stop_bits, int parity_en)
{
    VALIDATE_BUS(bus);

    if (config[bus].initialized) {
        M_ERROR("Bus %d is already initialized. To reinitialize, please close it first.\n", bus);
        return -1;
    }

    int rval;
    void* priv_data;

    if (get_uart_loc_from_bus(bus) == SLPI) {
        // TODO: return error for unsupported canonical / stop bits / parity on SLPI
        // slpi is in 8n1: non canonical, 8 bits/byte, no parity, 1 stop bit
        priv_data = malloc(sizeof(slpi_uart_t));
        rval = p_slpi_uart_init(bus, baud, (slpi_uart_t *) priv_data);
    }
    else {
        priv_data = malloc(sizeof(apps_uart_t));
        rval = p_apps_uart_init(bus, baud, canonical_en, stop_bits,
                                parity_en, (apps_uart_t *) priv_data);
    }

    if (rval == 0) {
        M_VERBOSE("Initialized bus %d", bus);
        config[bus] = (common_uart_config_t) {
            .initialized  = true,
            .should_close = false,
            .baud    = baud,
            .data    = priv_data,
            .timeout_millis = (int) (1000.0f * timeout_sec),
        };
    }
    else {
        M_ERROR("Encountered error while initializing bus %d\n", bus);
        free(priv_data);
    }

    return rval;
}


int voxl_uart_init_legacy_slpi(int bus, int baudrate)
{
    return voxl_uart_init(bus, baudrate, SLPI_TIMEOUT_WAIT_S, 0, 1, 0);
}


static int p_apps_uart_close(const int bus, apps_uart_t* self) {
    // silence unused variable
    (void) bus;

    // swallow error
    if (tcflush(self->fd, TCIOFLUSH) != 0) {
        M_ERROR("Unable to flush bus %d on close, data may be lost\n", bus);
        perror("Unable to flush bus on close, data may be lost");
    }
    
    if (close(self->fd) != 0) {
        M_ERROR("Encountered error while closing apps uart on bus %d\n", bus);
        perror("Encountered error while closing apps uart");
    }
    self->fd = 0;
    return 0;
}


static int p_slpi_uart_close(UNUSED const int bus, UNUSED slpi_uart_t* self) {
    pthread_mutex_lock(&slpi_init_mutex);
    slpi_in_use = false;
    ring_buf_clear(slpi_buf);
    pthread_mutex_unlock(&slpi_init_mutex);

    return 0;
}


int voxl_uart_close(const int bus) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    // set these flags to terminate ongoing reads / writes
    config[bus].initialized  = false;
    config[bus].should_close = true;

    int rval;
    if (get_uart_loc_from_bus(bus) == SLPI) {
        rval = p_slpi_uart_close(bus, (slpi_uart_t *) config[bus].data);
    }
    else {
        rval = p_apps_uart_close(bus, (apps_uart_t *) config[bus].data);
    }

    if (rval != 0) {
        M_ERROR("Got non-zero return code %d while closing UART (bus %d)\n", rval, bus);
    }
    free(config[bus].data);
    config[bus].baud = 0;

    return rval;
}


static int p_slpi_uart_write(UNUSED const int bus, uint8_t const* data, const size_t bytes,
                             UNUSED slpi_uart_t* self) {
    // TODO: implement slpi uart write in a fashion that allows for simultaneous
    // use of all 3 SLPI uarts. This will require refactoring the blob that runs
    // on SLPI, as it currently (so far as I can tell) only supports one UART at
    // a time (since there is only one topic for UART rx/tx)

    if(bytes>1024){
        M_ERROR("Encountered error while sending UART write data to SLPI\n");
        M_ERROR("SLPI can only send up to 1024 bytes at a time\n");
        M_ERROR("please break up your write into chunks\n");
        return -1;
    }

    int rval = fc_sensor_send_data("uart_data", data, bytes);
    if (rval != 0) {
        M_ERROR("Encountered error while sending write data to SLPI\n");
        return -1;
    }

    return (int) bytes;
}

static int p_apps_uart_write(const int bus, uint8_t const* data, const size_t bytes, apps_uart_t* self) {
    ssize_t rval = write(self->fd, data, bytes);

    if (rval == -1) {
        perror("Encountered error while writing to apps uart dev file");
        int errsv = errno;
        M_ERROR("Encountered error while writing to apps uart dev file on bus %d: %s (errno=%d)\n", bus,
            strerror(errsv), errsv);
    }

    return (int) rval;
}


int voxl_uart_write(const int bus, uint8_t const* data, const size_t bytes) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (bytes < 1) {
        M_ERROR("voxl_uart_write: bytes must be > 0\n");
        return -1;
    }

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_write(bus, data, bytes, (slpi_uart_t *) config[bus].data);
    }
    else {
        return p_apps_uart_write(bus, data, bytes, (apps_uart_t *) config[bus].data);
    }
}


static int p_slpi_uart_read_bytes(UNUSED const int bus, uint8_t* data, const size_t num_bytes,
                                  UNUSED slpi_uart_t* self) {
    ring_buf_status_t res;
    if (config[bus].timeout_millis != 0) {
        res = ring_buf_pop_timeout(slpi_buf, data, num_bytes,
                                   config[bus].timeout_millis);
        if (res < 0) {
            M_ERROR("Received error from ring_buf_pop_timeout: %s", ring_buf_status_str(res));
            return -1;
        }

    }
    else {
        res = ring_buf_pop(slpi_buf, data, num_bytes);
        if (res < 0) {
            M_ERROR("Received error from ring_buf_pop: %s", ring_buf_status_str(res));
            return -1;
        }
    }

    return (int) res;
}


static int p_apps_uart_read_bytes(const int bus, uint8_t* buf, const size_t num_bytes, apps_uart_t* self) {
    struct pollfd fds = { 0 };

    uint64_t timeout_millis;
    if (config[bus].timeout_millis == 0) timeout_millis = -1;
    else                                 timeout_millis = config[bus].timeout_millis;

    fds.fd = self->fd;
    fds.events = POLLIN;

    int rval = poll(&fds, 1, (int) timeout_millis);

    if (rval == -1) {
        if (errno != EINTR) {
            int errsv = errno;
            M_ERROR("Caught error while polling apps uart: %s (errno=%d)\n",
                    strerror(errsv), errsv);
        }
        M_DEBUG("Caught interrupt while polling apps uart\n");
        return -1;
    }
    else if (rval == 0) {
        return 0;
    }

    // read however many bytes are available
    int bytes_available;
    rval = ioctl(self->fd, FIONREAD, &bytes_available);
    if (rval == -1) {
        M_ERROR("Unable to execute FIONREAD on UART fd\n");
        return -1;
    }

    size_t read_chunk_size;
    if (bytes_available <= (int) num_bytes)    read_chunk_size = bytes_available;
    else                                       read_chunk_size = num_bytes;

    ssize_t bytes_read = read(self->fd, buf, read_chunk_size);
    if (bytes_read == -1) {
        int errsv = errno;
        M_ERROR("Received error while reading from apps uart: %s (errno=%d)\n",
                strerror(errsv), errsv);
        return -1;
    }

    return (int) bytes_read;
}


int voxl_uart_read_bytes(const int bus, uint8_t* data, const size_t num_bytes) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (num_bytes < 1) {
        M_ERROR("voxl_uart_read: max_len must be > 0\n");
        return -1;
    }

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_read_bytes(bus, data, num_bytes, (slpi_uart_t *) config[bus].data);
    }
    else {
        return p_apps_uart_read_bytes(bus, data, num_bytes, (apps_uart_t *) config[bus].data);
    }
}


static int p_slpi_uart_read_line(UNUSED const int bus, uint8_t* data, const size_t num_bytes,
                                 UNUSED slpi_uart_t* self) {
    int i;
    for (i = 0; (size_t) i < num_bytes; i++) {
        ring_buf_status_t res = ring_buf_pop(slpi_buf, &data[i], 1);
        if (res != OK) {
            M_ERROR("Encountered error '%s' while reading line from slpi uart, continuing...",
                    ring_buf_status_str(res));
            i--;
        }

        if (data[i] == '\n') {
            i++;
            break;
        }
    }

    return i;
}


static int p_apps_uart_read_line(const int bus, uint8_t* buf, const size_t num_bytes, apps_uart_t* self) {
    struct pollfd fds = { 0 };

    int timeout_millis;
    if (config[bus].timeout_millis == 0) timeout_millis = -1;
    else                                 timeout_millis = (int) config[bus].timeout_millis;

    size_t bytes_read = 0;
    size_t bytes_left = num_bytes;

    fds.fd = self->fd;
    fds.events = POLLIN;

    int rval;
    while (bytes_left > 0 && !config[bus].should_close) {
        rval = poll(&fds, 1, timeout_millis);

        if (rval == -1) {
            if (errno != EINTR) {
                int errsv = errno;
                M_ERROR("Caught error while polling apps uart: %s (errno=%d)\n",
                        strerror(errsv), errsv);
            }
            M_DEBUG("Caught interrupt while polling apps uart\n");
            return (int) bytes_read;
        }

        else if (rval == 0) {
            return (int) bytes_read;
        }

        else {
            // shadow rval with new type here
            uint8_t temp = 0;
            ssize_t rval2 = read(self->fd, &temp, 1);
            if (rval2 == -1) {
                int errsv = errno;
                M_ERROR("Received error while reading from apps uart: %s (errno=%d)\n",
                        strerror(errsv), errsv);
                return -1;
            }
            else if (temp == '\n') {
                return (int) bytes_read;
            }
            else {
                *(buf + bytes_read) = temp;
                bytes_read += rval2;
                bytes_left -= rval2;
            }
        }
    }

    return (int) bytes_read;
}


int voxl_uart_read_line(const int bus, uint8_t* buf, const size_t max_bytes) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (max_bytes < 1) {
        M_ERROR("voxl_uart_read_line: max_len must be > 0\n");
        return -1;
    }

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_read_line(bus, buf, max_bytes, (slpi_uart_t *) config[bus].data);
    }
    else {
        return p_apps_uart_read_line(bus, buf, max_bytes, (apps_uart_t *) config[bus].data);
    }
} 


static int p_slpi_uart_drain(UNUSED int bus, UNUSED slpi_uart_t* self) {
    return -1;
}

static int p_apps_uart_drain(UNUSED int bus, apps_uart_t* self) {
    int rval = tcdrain(self->fd);
    if (rval != 0) {
        int errsv = errno;
        char const* cause = strerror(errsv);
        M_ERROR("Unable to drain apps uart: %s (errno=%d)\n", cause, errsv);
        return -1;
    }
    return 0;
}

int voxl_uart_drain(const int bus) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_drain(bus, (slpi_uart_t*) config[bus].data);
    }
    else {
        return p_apps_uart_drain(bus, (apps_uart_t*) config[bus].data);
    }
}

// can't flush SLPI uart, so just return error for now
static int p_slpi_uart_flush(UNUSED const int bus, UNUSED slpi_uart_t* self) {
    return ring_buf_clear(slpi_buf);
}

static int p_apps_uart_flush(UNUSED const int bus, apps_uart_t* self) {
    int rval = tcflush(self->fd, TCIOFLUSH);
    if (rval != 0) {
        int errsv = errno;
        char const* cause = strerror(errsv);
        M_ERROR("Unable to flush apps uart: %s (errno=%d)\n", cause, errsv);
        return -1;
    }
    return 0;
}

int voxl_uart_flush(const int bus) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_flush(bus, (slpi_uart_t*) config[bus].data);
    }
    else {
        return p_apps_uart_drain(bus, (apps_uart_t*) config[bus].data);
    }
}

static int p_slpi_uart_bytes_available(UNUSED const int bus, UNUSED slpi_uart_t* self) {
    return (int) ring_buf_size(slpi_buf);
}

static int p_apps_uart_bytes_available(UNUSED const int bus, apps_uart_t* self) {
    int bytes_available;
    int rval = ioctl(self->fd, FIONREAD, &bytes_available);
    if (rval == -1) {
        return -1;
    }
    return bytes_available;
}

int voxl_uart_bytes_available(const int bus) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    if (get_uart_loc_from_bus(bus) == SLPI) {
        return p_slpi_uart_bytes_available(bus, (slpi_uart_t*) config[bus].data);
    }
    else {
        return p_apps_uart_bytes_available(bus, (apps_uart_t*) config[bus].data);
    }
}


int voxl_uart_set_read_timeout(const int bus, const uint32_t timeout) {
    VALIDATE_BUS(bus);
    CHECK_BUS_INIT(bus);

    config[bus].timeout_millis = timeout;
    return 0;
}
