/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <poll.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <modal_journal.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <stdbool.h>

// Local includes
#include "voxl_io/gpio.h"


// this should never occur, just pleases my linter since it can't find modal_journal.h for some reason
#ifndef M_VERBOSE
#define M_VERBOSE printf
#endif
#ifndef M_DEBUG
#define M_DEBUG printf
#endif
#ifndef M_WARN
#define M_WARN printf
#endif
#ifndef M_ERROR
#define M_ERROR printf
#endif


// -----------------------------------------------------------------------------------------------------------------------------
//
// -----------------------------------------------------------------------------------------------------------------------------
#define GPIO_CHIP_OFFSET    1100
#define FILE_GPIO_EXPORT    "/sys/class/gpio/export"
#define FILE_GPIO_EDGE      "/sys/class/gpio/gpio%d/edge"
#define FILE_GPIO_VALUE     "/sys/class/gpio/gpio%d/value"
#define FILE_GPIO_DIRECTION "/sys/class/gpio/gpio%d/direction"


// -----------------------------------------------------------------------------------------------------------------------------
// Define pin configuration type
// -----------------------------------------------------------------------------------------------------------------------------
enum internal_gpio_dir {
    INP = 0,       // Pin direction is strictly INPUT
    OUT = 1,       // Pin direction is strictly OUTPUT
    ALL = 2,       // Pin direction supports both INPUT or OUTPUT
    UNK = 3,      // Pin direction is unknown
};


// -----------------------------------------------------------------------------------------------------------------------------
// Pin Configuration Table
// Some GPIO's have level shifters that won't allow a change in direction, so this lookup table is used to identify
// the specific setting of a pin.
// 
// TODO: This was an initial solution for a lookup table to label pin direction restrictions, investigate if there's
//       a better way
// -----------------------------------------------------------------------------------------------------------------------------
static enum internal_gpio_dir pin_direction_table[MAX_NUM_GPIO_PINS] = {
  // 0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21   22   23   24   25
    UNK, UNK, UNK, UNK, ALL, ALL, ALL, ALL, ALL, ALL, UNK, UNK, ALL, ALL, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK,
    //    26   27   28   29   30   31   32   33   34   35   36   37   38   39   40   41   42   43   44   45   46   47   48   49   50  
         UNK, UNK, ALL, ALL, ALL, ALL, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, OUT, OUT, UNK, UNK, UNK,
    //    51   52   53   54   55   56   57   58   59   60   61   62   63   64   65   66   67   68   69   70   71   72   73   74   75
         UNK, INP, OUT, OUT, OUT, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK,
    //    76   77   78   79   80   81   82   83   84   85   86   87   88   89   90   91   92   93   94   95   96   97   98   99  100
         UNK, UNK, UNK, UNK, UNK, UNK, OUT, OUT, OUT, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK,
    //   101  102  103  104  105  106  107  108  109  110  111  112  113  114  115  116  117  118  119  120  121  122  123  124  125
         UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, ALL, UNK, UNK, ALL, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, OUT, UNK,
    //   126  127  128  129  130  131  132  133  134  135  136  137  138  139  140  141  142  143  144  145  146  147  148  149  150
         UNK, UNK, UNK, UNK, UNK, OUT, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK,
    //   151  152  152  154  155  156  157  158  159  160  161  162  163  164  165  166  167  168  169  170  171  172  173  174  175
         UNK, ALL, ALL, ALL, ALL, UNK, OUT, UNK, OUT, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK, UNK,
    //   176  177  178  179
         UNK, UNK, UNK, UNK
};


struct gpio_callback_table_entry {
    pthread_mutex_t mutex;
    pthread_t       handle;
    interrupt_callback_t cb;
    edge_type_t     edge;
    bool            in_use;
    bool            should_exit;
    int             gpio_file_fd;
};

static struct gpio_callback_table_entry callback_table[MAX_NUM_GPIO_PINS] = {};

#define VALIDATE_PIN_NUM(pin) if (pin >= MAX_NUM_GPIO_PINS) { \
    M_ERROR("GPIO pin must be between 0 and %d, got %d\n", MAX_NUM_GPIO_PINS - 1, pin); \
    return -1;                                                                    \
}

#define VALIDATE_PIN_DIR(pin, dir) if (dir >= ALL || \
    ((int) pin_direction_table[pin] != (int) UNK                 \
    && (int) pin_direction_table[pin] != (int) ALL               \
    && (int) pin_direction_table[pin] != (int) dir)) { \
    M_ERROR("Invalid GPIO direction for pin %d: got %d\n", pin, dir);                                         \
    return -1;                                                  \
}


__attribute__((constructor))
static void constructor(void) {
    for (int i = 0; i < MAX_NUM_GPIO_PINS; i++) {
        callback_table[i] = (struct gpio_callback_table_entry) {
            .mutex  = PTHREAD_MUTEX_INITIALIZER,
            .handle = 0,
            .cb     = NULL,
            .edge   = FALLING,
            .in_use = false,
            .should_exit  = false,
            .gpio_file_fd = -1,
        };
    }
}



// -----------------------------------------------------------------------------------------------------------------------------
// Writes value to file
// -----------------------------------------------------------------------------------------------------------------------------
static int _file_write(char const* file_path, char const* write_buf, size_t write_size) {
    
    int32_t fd = open(file_path, O_WRONLY);
    if ( fd < 0 ) {
        M_ERROR("Failed to open path %s\n", file_path);
        return -1;
    }

    write(fd, write_buf, write_size);
    //M_DEBUG("Writing %s to %s\n", write_buf, file_path);
    close(fd);

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Reads value from file
// -----------------------------------------------------------------------------------------------------------------------------
static int _file_read(char *file_path, char *read_buf, size_t read_size) {
    
    int32_t fd = open(file_path, O_RDONLY);
    if ( fd < 0 ) {
        M_ERROR("Failed to open path %s\n", file_path);
        return -1;
    }

    read(fd, read_buf, read_size);
    M_DEBUG("Read %s from %s\n", read_buf, file_path);
    close(fd);

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Exports GPIO pin for use
// -----------------------------------------------------------------------------------------------------------------------------
static int _gpio_export(uint32_t gpio_pin) {

    // Don't export if pin is already exported, we can check this by seeing if the value file is present.
    char gpio_path[128] = {0};
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_VALUE, gpio_pin + GPIO_CHIP_OFFSET);
    if ( !access(gpio_path, F_OK) ) {
        return 0;
    }

    // Open export path
    char gpio_buf[5] = {0};
    snprintf(gpio_buf, sizeof(gpio_buf), "%d", gpio_pin + GPIO_CHIP_OFFSET);

    if ( _file_write(FILE_GPIO_EXPORT, gpio_buf, sizeof(gpio_buf)) )
        return -1;

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Sets pin direction
// -----------------------------------------------------------------------------------------------------------------------------
int voxl_gpio_set_direction(uint32_t gpio_pin, uint8_t direction) {
    VALIDATE_PIN_NUM(gpio_pin);
    VALIDATE_PIN_DIR(gpio_pin, direction);

    // Begin GPIO setup and configuration
    if ( _gpio_export(gpio_pin) == -1 )
        return -1;

    // Open direction path
    char write_buf[5] = {0};
    char gpio_path[128] = {0};
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_DIRECTION, gpio_pin + GPIO_CHIP_OFFSET);

    // Get direction
    if (direction == GPIO_DIRECTION_INPUT) 
        snprintf(write_buf, sizeof(write_buf), "%s", "in");
    else if (direction == GPIO_DIRECTION_OUTPUT) 
        snprintf(write_buf, sizeof(write_buf), "%s", "out");
    else {
        M_ERROR("GPIO direction unsupported, must be 0 or 1: %d\n", direction);
        return -1;
    }

    // write
    if ( _file_write(gpio_path, write_buf, sizeof(write_buf)) )
        return -1;

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Writes value for GPIO pin
// -----------------------------------------------------------------------------------------------------------------------------
int voxl_gpio_write(uint32_t gpio_pin, uint8_t value) {
    VALIDATE_PIN_NUM(gpio_pin);

    if (value != 0 && value != 1) {
        M_ERROR("GPIO write value must be either 0 or 1: %d\n", value);
        return -1;
    }

    if ( voxl_gpio_set_direction(gpio_pin, GPIO_DIRECTION_OUTPUT) )
        return -1;

    // Configure tx buf and path
    char write_buf[5] = {0};
    sprintf(write_buf, "%d", value); 

    char gpio_path[128] = {0};
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_VALUE, gpio_pin + GPIO_CHIP_OFFSET);

    // write
    if ( _file_write(gpio_path, write_buf, sizeof(write_buf)) )
        return -1;

    return 0;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Reads value from GPIO pin
// -----------------------------------------------------------------------------------------------------------------------------
int voxl_gpio_read(uint32_t gpio_pin) {
    VALIDATE_PIN_NUM(gpio_pin);

    // TODO: need to decide if read should be handling direction or let user manually set direction
    //       since a change in direction will reset the pin value to 0
    // ari 1/10/24: reading shouldn't set GPIO to input in case we want to check
    // the value of an output pin
    // if ( voxl_gpio_set_direction(gpio_pin, GPIO_DIRECTION_INPUT) )
    //     M_DEBUG("GPIO direction could not be set, not required for read. Continuing\n");

    char gpio_path[128] = {0};
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_VALUE, gpio_pin + GPIO_CHIP_OFFSET);

    int gpio_value;
    char read_buf[2] = {0};
    if ( _file_read(gpio_path , read_buf, sizeof(read_buf)) )
        return -1;
    gpio_value = atoi(read_buf);
    M_DEBUG("Read value %d from GPIO pin %d\n", gpio_value, gpio_pin);

    return gpio_value;
}


static void* irq_cb_thread_target(void* target_thread_as_ptr) {
    uintptr_t target_pin_uintptr = (uintptr_t) target_thread_as_ptr;
    assert(target_pin_uintptr < UINT32_MAX);
    uint32_t target_pin = (uint32_t) target_pin_uintptr;

    // assume pin is valid
    // we will validate before starting this thread, in voxl_gpio_register_callback
    struct gpio_callback_table_entry* cb = &callback_table[target_pin];

    struct pollfd pfd;
    pthread_mutex_lock(&cb->mutex);
    if (cb->should_exit) {
        pthread_mutex_unlock(&cb->mutex);
        return NULL;
    }
    pfd.fd = cb->gpio_file_fd;
    pthread_mutex_unlock(&cb->mutex);
    pfd.events = POLLPRI;


    // loop infinitely so we can explicitly acquire mutex to check condition
    // will break if condition indicates we should exit
    while(1) {
        pthread_mutex_lock(&cb->mutex);
        if (cb->should_exit) {
            // need to clean up cb, will unlock mutex before returning
            break;
        }

        pthread_mutex_unlock(&cb->mutex);
        if ( poll(&pfd, 1, 100) == -1 ) {
            M_ERROR("Failed to poll\n");
            continue;
        }

        if ( pfd.revents & POLLPRI ) {
            int8_t read_buf[16];
            ssize_t len;

            if ( lseek(pfd.fd, 0, SEEK_SET) == -1 ) {
                M_ERROR("seek failed\n");
                continue;
            }

            len = read(pfd.fd, read_buf, sizeof(read_buf));

            if ( len == -1 ) {
                M_ERROR("Failed to read\n");
                continue;
            }

            read_buf[len] = 0;
            M_DEBUG("Read '%s'\n", read_buf);
            int pin_state_as_int = atoi((char*)read_buf);
            M_DEBUG("Converted read value to int %d\n", pin_state_as_int);


            pthread_mutex_lock(&cb->mutex);
            if (cb->should_exit) {
                pthread_mutex_unlock(&cb->mutex);
                break;
            }
            M_VERBOSE("Callback edge type is %d\n", cb->edge);
            if ((cb->edge == FALLING && pin_state_as_int == 0)
              || (cb->edge == RISING && pin_state_as_int == 1)
              || (cb->edge == BOTH)) {
                M_VERBOSE("Issuing callback\n");
                cb->cb(target_pin, pin_state_as_int);
            }
            pthread_mutex_unlock(&cb->mutex);
        }
    }

    cb->in_use = false;
    cb->should_exit = false;
    pthread_mutex_unlock(&cb->mutex);

    return NULL;
}


int voxl_gpio_set_edge_callback(uint32_t gpio_pin, edge_type_t edge, interrupt_callback_t callback) {
    VALIDATE_PIN_NUM(gpio_pin);
    struct gpio_callback_table_entry* cb = &callback_table[gpio_pin];

    pthread_mutex_lock(&cb->mutex);
    if (cb->in_use) {
        cb->should_exit = true;
        pthread_mutex_unlock(&cb->mutex);
        pthread_join(cb->handle, NULL);
        if (cb->in_use) {
            M_ERROR("old callback thread did not exit cleanly\n");
            return -1;
        }
        pthread_mutex_lock(&cb->mutex);
    }

    // export pin
    if ( _gpio_export(gpio_pin) == -1 )
        goto CLEANUP_EXIT_ERR;

    char write_buf[5] = {0};
    char gpio_path[128] = {0};

    // Write GPIO edge
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_EDGE, gpio_pin + GPIO_CHIP_OFFSET);
    int fd = open(gpio_path, O_WRONLY);
    if ( fd < 0 ) {
        printf("failed to open edge file\n");
        M_ERROR("Failed to open file %s\n", gpio_path);
        goto CLEANUP_EXIT_ERR;
    }

    memset(write_buf, 0, sizeof(write_buf));
    snprintf(write_buf, sizeof(write_buf), "%s", "both");

    write(fd, write_buf, sizeof(write_buf));
    close(fd);


    // Write GPIO value
    snprintf(gpio_path, sizeof(gpio_path), FILE_GPIO_VALUE, gpio_pin + GPIO_CHIP_OFFSET);
    fd = open(gpio_path, O_RDONLY);
    if ( fd < 0 ) {
        M_ERROR("Failed to open file %s\n", gpio_path);
        goto CLEANUP_EXIT_ERR;
    }


    cb->in_use = true;
    cb->should_exit = false;
    cb->cb = callback;
    cb->gpio_file_fd = fd;
    cb->edge = edge;

    uintptr_t arg = (uintptr_t) gpio_pin;
    int rval = pthread_create(&cb->handle, NULL, irq_cb_thread_target, (void*) arg);
    if (rval == -1) {
        M_ERROR("Unable to create IRQ callback thread\n");
        goto CLEANUP_EXIT_ERR;
    }

    pthread_mutex_unlock(&cb->mutex);
    return 0;


CLEANUP_EXIT_ERR:
    cb->in_use = false;
    cb->should_exit = true;
    cb->cb = NULL;
    cb->handle = 0;
    cb->gpio_file_fd = -1;
    pthread_mutex_unlock(&cb->mutex);
    return -1;
}

int voxl_gpio_remove_edge_callback(uint32_t pin) {
    VALIDATE_PIN_NUM(pin);
    struct gpio_callback_table_entry* cb = &callback_table[pin];

    pthread_mutex_lock(&cb->mutex);
    if (cb->in_use) {
        cb->should_exit = true;
        pthread_mutex_unlock(&cb->mutex);
        pthread_join(cb->handle, NULL);
        if (cb->in_use) {
            M_ERROR("old callback thread did not exit cleanly\n");
            return -1;
        }
        pthread_mutex_lock(&cb->mutex);
    }

    cb->in_use = false;
    cb->should_exit = true;
    cb->cb = NULL;
    cb->handle = 0;
    cb->gpio_file_fd = -1;
    pthread_mutex_unlock(&cb->mutex);
    return 0;
}
