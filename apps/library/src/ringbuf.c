//
// Created by ari on 9/14/23.
//

#include <malloc.h>
#include <memory.h>
#include "private/ringbuf.h"

char const* ring_buf_status_str(ring_buf_status_t status) {
    switch (status) {
        case OK:
            return "OK";
        case ERR_NO_SPACE:
            return "ERR_NO_SPACE";
        case ERR_OTHER:
            return "ERR_OTHER";
        default:
            return "unknown error value";
    }
}


/**
 * Get the amount of contiguous memory writeable starting at `head`. If `head > tail`, this will be `max_size - head`;
 * otherwise, it will be `tail - head`.
 * @param self The ring buffer we want to inspect
 * @return The amount of contiguous memory writeable after `head`
 */
static inline size_t _ring_buf_get_contig_writeable(ring_buf_t* self)
{
    if (self->head >= self->tail) {
        return self->max_size - self->head;
    }
    else {
        return self->tail - self->head;
    }
}

/**
 * Get the number of bytes which are contiguously readable starting at `tail`.
 * @param self The ring buffer to inspect
 * @return The number of contiguously readable bytes starting at `tail`.
 */
static inline size_t _ring_buf_get_contig_readable(ring_buf_t* self) {
    if (self->tail > self->head) {
        return self->max_size - self->tail;
    }
    else {
        return self->head - self->tail;
    }
}


static inline void _ring_buf_incr_head(ring_buf_t* self, size_t amt) {
    self->head = (self->head + amt) % self->max_size;
}


static inline void _ring_buf_incr_tail(ring_buf_t* self, size_t amt) {
    self->tail = (self->tail + amt) % self->max_size;
}


static inline size_t _ring_buf_avail_size(ring_buf_t* self) {
    return self->max_size - self->size;
}


static inline size_t _ring_buf_size(ring_buf_t* self) {
    return self->size;
}

ring_buf_t* ring_buf_create(size_t max_size) {
    ring_buf_t* buf = malloc(sizeof(ring_buf_t) + max_size);

    buf->head = 0;
    buf->tail = 0;
    buf->size = 0;
    buf->max_size = max_size;

    buf->mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    pthread_condattr_t cond_attr;
    pthread_condattr_init(&cond_attr);
    pthread_condattr_setclock(&cond_attr, CLOCK_MONOTONIC);
    pthread_cond_init(&buf->data_cond, &cond_attr);
    pthread_condattr_destroy(&cond_attr);

    return buf;
}


ring_buf_status_t ring_buf_push(ring_buf_t* __restrict__ self, uint8_t const* __restrict__ data, size_t length)
{
    if (pthread_mutex_lock(&self->mutex))   return ERR_OTHER;
    if (length > _ring_buf_avail_size(self)) {
        pthread_mutex_unlock(&self->mutex);
        return ERR_NO_SPACE;
    }

    size_t write1_space = _ring_buf_get_contig_writeable(self);

    // we only need one write
    if (write1_space >= length) {
        memcpy((void*) &self->buf[self->head], (void*) data, length);
        _ring_buf_incr_head(self, length);
    }
    // we need two writes
    else {
        // perform first write
        memcpy((void*) &self->buf[self->head], (void*) data, write1_space);
        _ring_buf_incr_head(self, write1_space);
        // perform second write
        memcpy((void*) &self->buf[self->head], (void*) (data + write1_space), length - write1_space);
        _ring_buf_incr_head(self, length - write1_space);
    }


    self->size += length;
    pthread_mutex_unlock(&self->mutex);
    pthread_cond_signal(&self->data_cond);

    return OK;
}


static ring_buf_status_t _ring_buf_pop_impl(ring_buf_t* __restrict__ self, \
                    uint8_t* __restrict__ dest_buf, size_t max_length)
{
    size_t total_read;
    size_t read1_space = _ring_buf_get_contig_readable(self);

    // shortcut for more continuous data available then requested
    if (read1_space >= max_length) {
        memcpy((void*) dest_buf, (void*) &self->buf[self->tail], max_length);
        _ring_buf_incr_tail(self, max_length);
        total_read = max_length;
        self->size -= total_read;
        return total_read;
    }

    // read the first chunk, usually this is all that's needed
    memcpy((void*) dest_buf, (void*) &self->buf[self->tail], read1_space);
    _ring_buf_incr_tail(self, read1_space);
    total_read = read1_space;

    // check if we need to do a second read due to looparound
    size_t remaining_space = max_length - read1_space;
    size_t read2_avail = _ring_buf_get_contig_readable(self);
    size_t read2_len = read2_avail;;
    if (read2_avail >= remaining_space){
        read2_len = remaining_space;
    }

    if(read2_len>0){
        memcpy((void*)(&dest_buf[read1_space]), &self->buf[self->tail], read2_len);
        _ring_buf_incr_tail(self, read2_len);
        total_read += read2_len;
    }

    self->size -= total_read;
    return total_read;
}


static void _ring_buf_clear(ring_buf_t* self) {
    self->tail = 0;
    self->head = 0;
    self->size = 0;
}


ring_buf_status_t ring_buf_clear(ring_buf_t* self) {
    if (pthread_mutex_lock(&self->mutex)) return ERR_OTHER;
    _ring_buf_clear(self);
    pthread_mutex_unlock(&self->mutex);

    return OK;
}


ring_buf_status_t ring_buf_pop(ring_buf_t* __restrict__ self, uint8_t* __restrict__ dest_buf, size_t max_length) {
    if (pthread_mutex_lock(&self->mutex)) return ERR_OTHER;
    while (_ring_buf_size(self) == 0) {
        pthread_cond_wait(&self->data_cond, &self->mutex);
    }

    ring_buf_status_t res = _ring_buf_pop_impl(self, dest_buf, max_length);

    pthread_mutex_unlock(&self->mutex);
    return res;
}


ring_buf_status_t ring_buf_pop_timeout(ring_buf_t* __restrict__ self, uint8_t* __restrict__ dest_buf, size_t max_length,
                                       uint32_t timeout_ms) {
    if (pthread_mutex_lock(&self->mutex)) return ERR_OTHER;

    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    ts.tv_nsec += 1000000 * timeout_ms;
    ts.tv_sec += ts.tv_nsec / 1000000000;
    ts.tv_nsec %= 1000000000;
    if (_ring_buf_size(self) == 0) {
        pthread_cond_timedwait(&self->data_cond, &self->mutex, &ts);
    }

    if (_ring_buf_size(self) == 0) {
        pthread_mutex_unlock(&self->mutex);
        return 0;
    }

    ring_buf_status_t res = _ring_buf_pop_impl(self, dest_buf, max_length);

    pthread_mutex_unlock(&self->mutex);
    return res;
}


size_t ring_buf_size(ring_buf_t* self) {
    pthread_mutex_lock(&self->mutex);
    size_t size = _ring_buf_size(self);
    pthread_mutex_unlock(&self->mutex);
    return size;
}


size_t ring_buf_avail_size(ring_buf_t* self) {
    pthread_mutex_lock(&self->mutex);
    size_t avail = _ring_buf_avail_size(self);
    pthread_mutex_unlock(&self->mutex);
    return avail;
}


ring_buf_status_t ring_buf_free(ring_buf_t* self) {
    pthread_cond_destroy(&self->data_cond);
    pthread_mutex_destroy(&self->mutex);

    return OK;
}
