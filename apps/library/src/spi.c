/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>

#include "voxl_io.h"

// #define DEBUG_FIFO

#define MAX_BUS 20
#define SPI_BASE_PATH "/dev/spidev"

// DSPL used to limit SPI reads to 512 bytes on APQ8096
// we don't seem to have that limit on QRB5165. This buffer is just a place
// to read IMU data into that's difficiently big for that.
#define SPI_BUFFER_LEN (64*1024)

typedef struct rc_spi_state_t {
    int init;
    int speed;
    int fd;
} rc_spi_state_t;

// state of all busses
static rc_spi_state_t state[MAX_BUS + 1];
// read buffer for spi allocated on heap at init?
static uint8_t* imu_read_buf[MAX_BUS + 1];


static int __open_fd(int bus)
{
    char buf[32];
    int ret;

    // hardcoded for now?
    int slave = 0;

    snprintf(buf, sizeof(buf), SPI_BASE_PATH "%d.%d", bus, slave);
    // open file descriptor
    ret = open(buf, O_RDWR);

    if(ret == -1) {
        perror("ERROR in voxl_spi_init, failed to open /dev/spidev device");
        if(errno != EPERM) fprintf(stderr, "likely SPI is not enabled in the device tree or kernel\n");
        return -1;
    }
    return ret;
}

int voxl_spi_init(int bus, int bus_mode, int freq_hz)
{
    int bits = VOXL_SPI_BITS_PER_WORD;
    int fd;

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_init, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(freq_hz > VOXL_SPI_MAX_SPEED || freq_hz < VOXL_SPI_MIN_SPEED) {
        fprintf(stderr, "ERROR in voxl_spi_init, speed_hz must be between %d & %d\n", VOXL_SPI_MIN_SPEED, VOXL_SPI_MAX_SPEED);
        return -1;
    }
    if(bus_mode != SPI_MODE_0 && bus_mode != SPI_MODE_1 && bus_mode != SPI_MODE_2 && bus_mode != SPI_MODE_3) {
        fprintf(stderr, "ERROR in voxl_spi_init, bus_mode must be SPI_MODE_0, 1, 2, or 3\n");
        return -1;
    }

    // get file descriptor for spi1 device
    fd = __open_fd(bus);
    if(fd == -1) return -1;

    // set settings
    if(ioctl(fd, SPI_IOC_WR_MODE, &bus_mode) == -1) {
        perror("ERROR in voxl_spi_init setting spi wr mode");
        close(fd);
        return -1;
    }
    if(ioctl(fd, SPI_IOC_RD_MODE, &bus_mode) == -1) {
        perror("ERROR in voxl_spi_init setting spi rd mode");
        close(fd);
        return -1;
    }
    if(ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits) == -1) {
        perror("ERROR in voxl_spi_init setting wr bits per word");
        close(fd);
        return -1;
    }
    if(ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits) == -1) {
        perror("ERROR in voxl_spi_init setting rd bits per word");
        close(fd);
        return -1;
    }
    if(ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &freq_hz) == -1) {
        perror("ERROR in voxl_spi_init setting max wr speed hz");
        close(fd);
        return -1;
    }
    if(ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &freq_hz) == -1) {
        perror("ERROR in voxl_spi_init setting max rd speed hz");
        close(fd);
        return -1;
    }

    // all done, store speed and flag initialization
    state[bus].init = 1;
    state[bus].fd = fd;
    state[bus].speed = freq_hz;

    return 0;
}


int voxl_spi_get_fd(int bus)
{
    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_get_fd, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_get_fd, need to initialize first\n");
        return -1;
    }
    return state[bus].fd;
}


int voxl_spi_set_freq(int bus, int freq_hz)
{
    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_get_fd, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_get_fd, need to initialize first\n");
        return -1;
    }
    state[bus].speed = freq_hz;
    return 0;
}


int voxl_spi_close(int bus)
{
    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in rc_spi_close, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }

    if(state[bus].init == 1) {
        close(state[bus].fd);
    }

    // make sure struct is wiped
    state[bus].init = 0;
    state[bus].fd = 0;
    state[bus].speed = 0;

    return 0;
}


int voxl_spi_transfer(int bus, uint8_t* tx_data, size_t tx_bytes, uint8_t* rx_data)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_transfer, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_transfer, need to initialize first\n");
        return -1;
    }
    if(tx_bytes < 1) {
        fprintf(stderr, "ERROR: in voxl_spi_transfer, tx_bytes must be >=1\n");
        return -1;
    }

    // fill in send struct
    xfer.tx_buf = (unsigned long)tx_data;
    xfer.rx_buf = (unsigned long)rx_data;
    xfer.len = tx_bytes;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // do ioctl transfer
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_transfer");
        return -1;
    }
    return 0;
}


int voxl_spi_write(int bus, uint8_t* data, size_t bytes)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_write, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_write, need to initialize first\n");
        return -1;
    }
    if(bytes < 1) {
        fprintf(stderr, "ERROR: in voxl_spi_write, bytes must be >=1\n");
        return -1;
    }

    uint8_t buf[2] = {0};

    // fill in send struct
    xfer.tx_buf = (unsigned long)data;
    xfer.rx_buf = (unsigned long)buf;
    xfer.len = bytes;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // send
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_write");
        return -1;
    }
    return 0;
}


int voxl_spi_write_reg_byte(int bus, uint8_t address, uint8_t val)
{
    uint8_t buf[2] = {address, val};

    if(voxl_spi_write(bus, buf, 2)) {
        perror("ERROR in voxl_spi_write_reg_byte");
        return -1;
    }
    return 0;
}


int voxl_spi_write_reg_word(int32_t bus, uint8_t address, uint16_t val)
{
    uint8_t buf[3] = {address, (val >> 8), (val & 0xFF)};

    if(voxl_spi_write(bus, buf, 3)) {
        perror("ERROR in voxl_spi_write_reg_word");
        return -1;
    }
    return 0;
}


int voxl_spi_read(int bus, uint8_t* data, size_t bytes)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_read, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_read, need to initialize first\n");
        return -1;
    }
    if(bytes < 1) {
        fprintf(stderr, "ERROR: in voxl_spi_read, bytes must be >=1\n");
        return -1;
    }

    // fill in send struct
    xfer.tx_buf = 0;
    xfer.rx_buf = (unsigned long)data;
    xfer.len = bytes;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // read
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_read");
        return -1;
    }
    return ret;
}


// needs double checking
int voxl_spi_read_reg(int bus, uint8_t address, uint8_t* out_buf, int length)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg, bus must be between 0 and %d\n", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg, need to initialize first\n");
        return -1;
    }
    if(length < 1) {
        fprintf(stderr, "ERROR: in voxl_spi_read_reg, bytes must be >=1\n");
        return -1;
    }

    int transfer_bytes = 1 + length;  // first byte is address

    uint8_t tx_buf[1];
    uint8_t rx_buf[transfer_bytes];   // 1 extra for address receive?

    tx_buf[0] = address | 0x80; // register high bit=1 for read

    // fill in send struct
    xfer.tx_buf = (unsigned long)tx_buf;
    xfer.rx_buf = (unsigned long)rx_buf;
    xfer.len = transfer_bytes;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // read
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_read");
        return -1;
    }
    // skip the first (dummy) byte and read out the rest
    memcpy(out_buf, &(rx_buf[1]), length);
    return 0;
}


int voxl_spi_read_reg_byte(int bus, uint8_t address, uint8_t* out)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg_byte, bus must be between 0 & %d", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg_byte, need to initialize first\n");
        return -1;
    }
    uint8_t tx_buf[1];          // one byte for address
    tx_buf[0] = address | 0x80; // register high bit=1 for read

    uint8_t rx_buf[2] = {0};    // extra byte for address, then read register value

    // fill in send struct
    xfer.tx_buf = (unsigned long)tx_buf;
    xfer.rx_buf = (unsigned long)rx_buf;
    xfer.len = 2;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // read
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_read_reg_byte");
        return -1;
    }

    *out = rx_buf[1];
    return 0;
}


int voxl_spi_read_reg_word(int bus, uint8_t address, uint16_t* out)
{
    int ret;
    struct spi_ioc_transfer xfer = {0};  // zero-initialize per docs

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg_word, bus must be between 0 & %d", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_read_reg_word, need to initialize first\n");
        return -1;
    }

    int transfer_bytes = 3;  // first byte is address
    uint8_t tx_buf[1];       // one byte for address
    uint8_t rx_buf[transfer_bytes];

    tx_buf[0] = address | 0x80; // register high bit=1 for read

    // fill in send struct
    xfer.tx_buf = (unsigned long)tx_buf;
    xfer.rx_buf = (unsigned long)rx_buf;
    xfer.len = transfer_bytes;
    xfer.speed_hz = state[bus].speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // read
    ret = ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer);
    if(ret == -1) {
        perror("ERROR in voxl_spi_read");
        return -1;
    }

    // skip the first (dummy) byte and read out the rest
    *out = (uint16_t) (rx_buf[1] << 8 | rx_buf[2]);
    return 0;
}


int voxl_spi_read_imu_fifo(int bus, uint8_t count_address, uint8_t fifo_address,
                           uint8_t packet_size, int* packets_read, uint8_t* data, 
                           int dataLen, int count_speed, int data_speed)
{
    int ret;
    *packets_read = 0;  // zero out packets read until we actually read
    uint16_t fifo_count = 0;

    // sanity checks
    if(bus < 0 || bus > MAX_BUS) {
        fprintf(stderr, "ERROR in voxl_spi_read_imu_fifo, bus must be between 0 & %d", MAX_BUS);
        return -1;
    }
    if(state[bus].init == 0) {
        fprintf(stderr, "ERROR in voxl_spi_read_imu_fifo, need to initialize first\n");
        return -1;
    }

    // malloc some memory for the imu buffer if it hasn't already
    if(imu_read_buf[bus] == NULL) {
        imu_read_buf[bus] = malloc(SPI_BUFFER_LEN);
        if(imu_read_buf[bus] == NULL) {
            fprintf(stderr, "ERROR in voxl_spi_init, failed to malloc read buf");
            return -1;
        }
    }

    // set speed for reading count address, this is fast, just a setter function
    voxl_spi_set_freq(bus, count_speed);

    // first read how many bytes are available using the fifo_count register
    ret = voxl_spi_read_reg_word(bus, count_address, &fifo_count);

    if(ret) {
        fprintf(stderr, "ERROR in voxl_spi_read_imu_fifo, failed to read count address %d", count_address);
        return -1;
    }

    if(fifo_count > dataLen) {
        fprintf(stderr, "WARNING in voxl_spi_read_imu_fifo, impossibly large fifo_count:%d", fifo_count);
        fprintf(stderr, "trying again");
        voxl_spi_read_reg_word(bus, count_address, &fifo_count);
        if(fifo_count > dataLen) return -1;
    }

    // packets available to read
    // this rounds down and warns ifa partial packet is in fifo
    int p_available = fifo_count / packet_size;

    // special case for icm42688, read an extra packet which may have been sampled
    // during the fifo read and check if it's valid after
    if(fifo_address == 48 && packet_size == 20 && p_available<=103){
        p_available += 1;
    }

    // no need to read the fifo if it is empty so just return now
    if(p_available == 0) return 0;

    // warn of partial packets, this is normal for MPU9250
    if(fifo_count % packet_size) {
        fprintf(stderr, "WARNING in io_rpc_spi_read_imu_fifo, fifo_count reported partial packets in fifo, count=%d", fifo_count);
    }

    // send buffer is the fifo register address
    uint8_t tx_buf[1];                   // one byte for address
    tx_buf[0] = fifo_address | 0x80;     // register high bit=1 for read

    // fill in transfer struct manually with the special faster data speed
    struct spi_ioc_transfer xfer = {0};
    xfer.tx_buf = (unsigned long)tx_buf;
    xfer.rx_buf = (unsigned long)imu_read_buf[bus];
    xfer.speed_hz = data_speed;
    xfer.delay_usecs = 0;
    xfer.bits_per_word = VOXL_SPI_BITS_PER_WORD;
    xfer.cs_change = 1;

    // bytes to read from fifo
    int b_to_read = p_available * packet_size;


    // set up ioctl transfer struct
    int transfer_bytes = 1 + b_to_read;  // first byte is address
    xfer.len = transfer_bytes;
    if(ioctl(state[bus].fd, SPI_IOC_MESSAGE(1), &xfer) != transfer_bytes) {
        perror("ERROR in io_rpc_spi_read_imu_fifo ioctl");
        return -1;
    }

    // special case for icm42688, we read an extra packet, see if it's valid
    if(fifo_address == 48 && packet_size == 20){

        // if we overrun the end of the fifo data (normal) it will show a bad
        // header so mark that as the end of the data
        int loc = (p_available-1)*20;
        if((imu_read_buf[bus][1+loc]&0xFC)!=120){
            p_available--;
            // printf("no extra\n");
        }
        else{
            // printf("got extra\n");
        }
    }

    // skip the first (dummy) byte and read out the rest
    memcpy(data, &(imu_read_buf[bus][1]), p_available*packet_size);


    // this was for debugging, read how many bytes are available after reading fifo
    // voxl_spi_set_freq(bus, count_speed);
    // ret = voxl_spi_read_reg_word(bus, count_address, &fifo_count);
    // printf("count left: %d\n", fifo_count);

    // let the user know how many packets got read
    *packets_read = p_available;
    return 0;
}
