#!/bin/bash
#
# Modal AI Inc. 2023
# author: james@modalai.com


sudo rm -rf build64
sudo rm -rf slpi/build/
sudo rm -rf apps/build/
sudo rm -rf pkg/control.tar.gz
sudo rm -rf pkg/data/
sudo rm -rf pkg/data.tar.gz
sudo rm -rf pkg/DEB/
sudo rm -rf pkg/IPK/
sudo rm -rf *.ipk
sudo rm -rf *.deb
sudo rm -rf .bash_history
