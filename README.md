# qrb5165-io

This build requires the rb5-flight-px4-build-docker docker image. The docker image
contains the Hexagon SDK needed to build the SLPI DSP portion of the code base and
a Linaro ARM SDK to build the applications processor portion of the code base. The
Hexagon SDK software license does not allow redistribution of any kind so it has
to be downloaded by each user individually and then built into a Docker. The instructions
for this process are in the following link:

https://gitlab.com/voxl-public/rb5-flight/rb5-flight-px4-build-docker

## Source

### examples

The following examples are supported ModalAI QRB5165 based platforms (VOXL2, VOXL2 Mini)

#### qrb5165io-spi-test

- An example that demos how to to check the `WHOAMI` register for an ICM-42688p IMU

#### qrb5165io-gpio-test

- An example that demos how to to set/get GPIO state

#### qrb5165io-uart-test

- An example that demos using application processor UARTs for harware loopback testing
